-- MySQL dump 10.13  Distrib 5.6.24, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: carlosjung
-- ------------------------------------------------------
-- Server version	5.7.8-rc-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `gl_bairrovo`
--

DROP TABLE IF EXISTS `gl_bairrovo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gl_bairrovo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` text,
  `idCidadeVO` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idCidadeVO` (`idCidadeVO`),
  KEY `idCidadeVO_2` (`idCidadeVO`),
  CONSTRAINT `FK660ACFA3BF87AEC5` FOREIGN KEY (`idCidadeVO`) REFERENCES `gl_cidadevo` (`id`),
  CONSTRAINT `FK80B16E49BF87AEC5` FOREIGN KEY (`idCidadeVO`) REFERENCES `gl_cidadevo` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gl_bairrovo`
--

LOCK TABLES `gl_bairrovo` WRITE;
/*!40000 ALTER TABLE `gl_bairrovo` DISABLE KEYS */;
/*!40000 ALTER TABLE `gl_bairrovo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gl_campotela`
--

DROP TABLE IF EXISTS `gl_campotela`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gl_campotela` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `destino` text,
  `nome` text,
  `classe` text,
  `idTela` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idTela_2` (`idTela`),
  KEY `idTela` (`idTela`),
  CONSTRAINT `FK6129207373E1DFF6` FOREIGN KEY (`idTela`) REFERENCES `gl_tela` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=509 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gl_campotela`
--

LOCK TABLES `gl_campotela` WRITE;
/*!40000 ALTER TABLE `gl_campotela` DISABLE KEYS */;
INSERT INTO `gl_campotela` VALUES (27,'txtDescricao','Código','',13),(206,'txtDescricao','Código','',16),(263,'txtNome','SMTP','',15),(264,'txtReferencia','E-mail','',15),(265,'txtValor','Porta','',15),(266,'txtOrdem','Senha','',15),(348,'txtMeta','Metatags Sociais','',14),(349,'txtDescricao','Código','',14),(350,'txtNome','Link','',35),(351,'txtDescricao','metatags','',35),(425,'txtNome','Link','',41),(426,'txtNome','Link','',42),(427,'txtNome','Link','',43),(428,'txtNome','Link','',44),(433,'txtNome','Telefone Fixo','',26),(434,'txtReferencia','Celular','',26),(435,'txtNome','Telefone Fixo','',5),(436,'txtReferencia','Celular','',5),(437,'txtDescricao','Informações','',5),(442,'txtNome','Título','',25),(443,'txtDescricao','Texto','',25),(444,'txtNome','Titulo','',46),(445,'txtDescricao','Texto','',46),(448,'txtOrdem','',NULL,23),(449,'txtDescricao','Texto','',47),(450,'DropSegmentoPai','Segmento','',47),(465,'txtNome','Nome','',48),(466,'txtOrdem','Ordem','',48),(467,'txtDescricao','Texto','',48),(468,'DropVisivel','Visível','',48),(469,'txtNome','Telefones','',11),(470,'txtReferencia','E-mail para Receber','',11),(471,'txtDescricao','Endereço','',11),(486,'txtDescricao','Texto','',51),(487,'txtNome','Nome','',50),(488,'txtReferencia','Cargo','',50),(489,'txtOrdem','Ordem','',50),(490,'txtDescricao','Texto','',50),(491,'DropVisivel','Visível','',50),(492,'txtNome','Titulo','',52),(493,'txtDescricao','Texto','',52),(494,'txtDescricao','Texto','',53),(498,'txtDescricao','Texto','',56),(499,'txtDescricao','Texto','',55),(500,'txtDescricao','Texto','',54),(505,'txtNome','Titulo','',49),(506,'txtData','Data','',49),(507,'txtDescricao','Texto','',49),(508,'DropVisivel','Visível','',49);
/*!40000 ALTER TABLE `gl_campotela` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gl_categoriavo`
--

DROP TABLE IF EXISTS `gl_categoriavo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gl_categoriavo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` text,
  `imagem` text,
  `descricao` text,
  `ordem` text,
  `chave` text,
  `visivel` tinyint(1) DEFAULT NULL,
  `idTela` int(11) DEFAULT NULL,
  `idSegmentoFilhoVO` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idTela` (`idTela`),
  KEY `idSegmentoFilhoVO` (`idSegmentoFilhoVO`),
  KEY `idTela_2` (`idTela`),
  KEY `idSegmentoFilhoVO_2` (`idSegmentoFilhoVO`),
  CONSTRAINT `FK2B5E242273E1DFF6` FOREIGN KEY (`idTela`) REFERENCES `gl_tela` (`id`),
  CONSTRAINT `FK2B5E242274AF9F36` FOREIGN KEY (`idSegmentoFilhoVO`) REFERENCES `gl_segmentofilhovo` (`id`),
  CONSTRAINT `FK39462B9873E1DFF6` FOREIGN KEY (`idTela`) REFERENCES `gl_tela` (`id`),
  CONSTRAINT `FK39462B9874AF9F36` FOREIGN KEY (`idSegmentoFilhoVO`) REFERENCES `gl_segmentofilhovo` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gl_categoriavo`
--

LOCK TABLES `gl_categoriavo` WRITE;
/*!40000 ALTER TABLE `gl_categoriavo` DISABLE KEYS */;
/*!40000 ALTER TABLE `gl_categoriavo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gl_cidadevo`
--

DROP TABLE IF EXISTS `gl_cidadevo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gl_cidadevo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` text,
  `status` text,
  `idEstadoVO` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idEstadoVO` (`idEstadoVO`),
  KEY `idEstadoVO_2` (`idEstadoVO`),
  CONSTRAINT `FK8F984182AC63FB69` FOREIGN KEY (`idEstadoVO`) REFERENCES `gl_estadovo` (`id`),
  CONSTRAINT `FKAE7EE028AC63FB69` FOREIGN KEY (`idEstadoVO`) REFERENCES `gl_estadovo` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gl_cidadevo`
--

LOCK TABLES `gl_cidadevo` WRITE;
/*!40000 ALTER TABLE `gl_cidadevo` DISABLE KEYS */;
/*!40000 ALTER TABLE `gl_cidadevo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gl_clientevo`
--

DROP TABLE IF EXISTS `gl_clientevo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gl_clientevo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` text,
  `razaoSocial` text,
  `inscricaoEstadual` text,
  `CPFCNPJ` text,
  `status` text,
  `dataInicio` datetime DEFAULT NULL,
  `dataFim` datetime DEFAULT NULL,
  `observacao` text,
  `idLoja` int(11) DEFAULT NULL,
  `idContatoVO` int(11) DEFAULT NULL,
  `idEnderecoVO` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idContatoVO` (`idContatoVO`),
  KEY `idEnderecoVO` (`idEnderecoVO`),
  KEY `idContatoVO_2` (`idContatoVO`),
  KEY `idEnderecoVO_2` (`idEnderecoVO`),
  CONSTRAINT `FK1233EC874CE8A6FF` FOREIGN KEY (`idEnderecoVO`) REFERENCES `gl_enderecovo` (`id`),
  CONSTRAINT `FK1233EC87FA64F7F` FOREIGN KEY (`idContatoVO`) REFERENCES `gl_contatovo` (`id`),
  CONSTRAINT `FKAA974594CE8A6FF` FOREIGN KEY (`idEnderecoVO`) REFERENCES `gl_enderecovo` (`id`),
  CONSTRAINT `FKAA97459FA64F7F` FOREIGN KEY (`idContatoVO`) REFERENCES `gl_contatovo` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gl_clientevo`
--

LOCK TABLES `gl_clientevo` WRITE;
/*!40000 ALTER TABLE `gl_clientevo` DISABLE KEYS */;
/*!40000 ALTER TABLE `gl_clientevo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gl_contatovo`
--

DROP TABLE IF EXISTS `gl_contatovo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gl_contatovo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email1` text,
  `email2` text,
  `responsavelEmail1` text,
  `responsavelEmail2` text,
  `fone1` text,
  `fone2` text,
  `fone3` text,
  `responsavelTelefones` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gl_contatovo`
--

LOCK TABLES `gl_contatovo` WRITE;
/*!40000 ALTER TABLE `gl_contatovo` DISABLE KEYS */;
/*!40000 ALTER TABLE `gl_contatovo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gl_dadovo`
--

DROP TABLE IF EXISTS `gl_dadovo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gl_dadovo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `referencia` text,
  `nome` text,
  `chave` text,
  `destaque` text,
  `descricao` text,
  `resumo` text,
  `valor` text,
  `ordem` text,
  `visivel` tinyint(1) DEFAULT NULL,
  `idSegmentoPaiVO` int(11) DEFAULT NULL,
  `idSegmentoFilhoVO` int(11) DEFAULT NULL,
  `idCategoriaVO` int(11) DEFAULT NULL,
  `idTela` int(11) DEFAULT NULL,
  `data` datetime DEFAULT NULL,
  `keywords` text,
  `meta` text,
  PRIMARY KEY (`id`),
  KEY `idSegmentoPaiVO` (`idSegmentoPaiVO`),
  KEY `idSegmentoFilhoVO` (`idSegmentoFilhoVO`),
  KEY `idCategoriaVO` (`idCategoriaVO`),
  KEY `idTela` (`idTela`),
  KEY `idSegmentoPaiVO_2` (`idSegmentoPaiVO`),
  KEY `idSegmentoFilhoVO_2` (`idSegmentoFilhoVO`),
  KEY `idCategoriaVO_2` (`idCategoriaVO`),
  KEY `idTela_2` (`idTela`),
  CONSTRAINT `FK44B82A9073E1DFF6` FOREIGN KEY (`idTela`) REFERENCES `gl_tela` (`id`),
  CONSTRAINT `FK44B82A9074AF9F36` FOREIGN KEY (`idSegmentoFilhoVO`) REFERENCES `gl_segmentofilhovo` (`id`),
  CONSTRAINT `FK44B82A90AEED9408` FOREIGN KEY (`idCategoriaVO`) REFERENCES `gl_categoriavo` (`id`),
  CONSTRAINT `FK44B82A90DE0F7C3F` FOREIGN KEY (`idSegmentoPaiVO`) REFERENCES `gl_segmentopaivo` (`id`),
  CONSTRAINT `FK7F58AACF73E1DFF6` FOREIGN KEY (`idTela`) REFERENCES `gl_tela` (`id`),
  CONSTRAINT `FK7F58AACF74AF9F36` FOREIGN KEY (`idSegmentoFilhoVO`) REFERENCES `gl_segmentofilhovo` (`id`),
  CONSTRAINT `FK7F58AACFAEED9408` FOREIGN KEY (`idCategoriaVO`) REFERENCES `gl_categoriavo` (`id`),
  CONSTRAINT `FK7F58AACFDE0F7C3F` FOREIGN KEY (`idSegmentoPaiVO`) REFERENCES `gl_segmentopaivo` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gl_dadovo`
--

LOCK TABLES `gl_dadovo` WRITE;
/*!40000 ALTER TABLE `gl_dadovo` DISABLE KEYS */;
INSERT INTO `gl_dadovo` VALUES (5,'9264-8888','(51) 3342.5036','51-3342-50360','T','Av. Farrapos, 2461<br />\r\nBairro Floresta | Porto Alegre/RS','<div>\r\n	51 | 3472.5977 | 3478.1171 | 3478.3025</div>\r\n<div>\r\n	 </div>\r\n<div>\r\n	Rua Saldanha da Gama, 296</div>\r\n<div>\r\n	Bairro Harmonia - Canoas/RS</div>','0','Telefones ',1,NULL,NULL,NULL,5,'0001-01-01 00:00:00','',''),(7,'','','','T','','','0','',1,NULL,NULL,NULL,16,'0001-01-01 00:00:00','',''),(10,'contato@formulariodosite.com.br','smtp.formulariodosite.com.br','smtp-formulariodosite-com-br','T','','','587','zpsite15!',1,NULL,NULL,NULL,15,'0001-01-01 00:00:00','',''),(13,'guilherme@zepol.com.br','(51) 3094.2300 - 3013.7800','51-3094-2300-3013-7800','T','Av. Ipiranga 40, salas 1801/1802<br />\r\nPraia de Belas - Porto Alegre/RS','Av. Farrapos, 2461<br />\r\nBairro Floresta - Porto Alegre/RS<br />\r\n<p>\r\n	<span style=\"font-size:26px;\">51 | 3342.5036</span></p>','0','',1,NULL,NULL,NULL,11,'0001-01-01 00:00:00','',''),(19,'','',NULL,'T','','','0',NULL,1,NULL,NULL,NULL,13,'0001-01-01 00:00:00',NULL,NULL),(34,'9264-8888','(51) 3342.5036','51-3342-5036','T','','','0','Telefones teste teste',1,NULL,NULL,NULL,26,'0001-01-01 00:00:00','',''),(37,'','SOBRE A EMPRESA','sobre-a-empresa','T','At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et ha rum quidem rerum facilis est et expedita distinctio. Nam libero tempore, c um soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus.','','0','',1,NULL,NULL,NULL,25,'0001-01-01 00:00:00','',''),(54,'','',NULL,'T','','','0',NULL,1,NULL,NULL,NULL,41,'0001-01-01 00:00:00',NULL,NULL),(55,'','Serviço prestados pela VF','servico-prestados-pela-vf','T','At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate n on provident, similique sunt in culpa qui officia deserunt molli tia animi, id est laborum et dolorum fuga. Quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi.','','0','',1,NULL,NULL,NULL,46,'0001-01-01 00:00:00','',''),(56,'','','2','T','At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mol litia animi, id est laborum et dolorum fuga','','0','',1,8,NULL,NULL,47,'0001-01-01 00:00:00','',''),(57,'','','3','T','At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mol litia animi, id est laborum et dolorum fuga','','0','',1,9,NULL,NULL,47,'0001-01-01 00:00:00','',''),(58,'de todos os tamanhos, masculina e feminina.','Fabricamos Camisas','fabricamos-camisas','T','','','0','',1,NULL,NULL,NULL,45,'0001-01-01 00:00:00','',''),(59,'quem-somos','Quem Somos','quem-somos','T','Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque...','','0','',1,NULL,NULL,NULL,48,'0001-01-01 00:00:00','',''),(60,'produtos','Nossos Produtos','nossos-produtos','T','Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque...','','0','3',1,NULL,NULL,NULL,48,'0001-01-01 00:00:00','',''),(61,'servicos','Serviços','servicos','T','Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque...','','0','2',1,NULL,NULL,NULL,48,'0001-01-01 00:00:00','',''),(62,'','Noticias 1','noticias-1','T','teste','','0','',1,NULL,NULL,NULL,49,'2015-10-10 00:00:00','',''),(63,'365464','Carlos Emílio Jung','carlos-emilio-jung','T','TEAFADASDASEWD','','0','1',1,NULL,NULL,NULL,50,'0001-01-01 00:00:00','',''),(64,'','DIREITO CIVIL','direito-civil','T','<span style=\"color: rgb(85, 85, 85); font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 22.4px;\">Consultoria e pareceres em matéria cível e de direito empresarial; </span><br style=\"box-sizing: border-box; color: rgb(85, 85, 85); font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 22.4px; border-radius: 0px !important;\" />\r\n<br style=\"box-sizing: border-box; color: rgb(85, 85, 85); font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 22.4px; border-radius: 0px !important;\" />\r\n<span style=\"color: rgb(85, 85, 85); font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 22.4px;\">Patrocínio dos interesses de empresas dos diversos segmentos econômicos perante em processos que tramitam perante a Justiça Estadual Comum e da Justiça Federal;</span><br style=\"box-sizing: border-box; color: rgb(85, 85, 85); font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 22.4px; border-radius: 0px !important;\" />\r\n<br style=\"box-sizing: border-box; color: rgb(85, 85, 85); font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 22.4px; border-radius: 0px !important;\" />\r\n<span style=\"color: rgb(85, 85, 85); font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 22.4px;\">Patrocínio dos interesses de empresas dos diversos segmentos econômicos] perante os Juizados Especiais Cíveis da Justiça Comum Estadual e Federal;Elaboração e revisão de contratos;</span><br style=\"box-sizing: border-box; color: rgb(85, 85, 85); font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 22.4px; border-radius: 0px !important;\" />\r\n<br style=\"box-sizing: border-box; color: rgb(85, 85, 85); font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 22.4px; border-radius: 0px !important;\" />\r\n<span style=\"color: rgb(85, 85, 85); font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 22.4px;\">Elaboração e revisão de contratos;</span><br style=\"box-sizing: border-box; color: rgb(85, 85, 85); font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 22.4px; border-radius: 0px !important;\" />\r\n<br style=\"box-sizing: border-box; color: rgb(85, 85, 85); font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 22.4px; border-radius: 0px !important;\" />\r\n<span style=\"color: rgb(85, 85, 85); font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 22.4px;\">Direito Civil do Transporte: (i) Lei nº 10.406, de 10 de janeiro de 2002 - Código Civil Brasileiro - Transporte de Pessoas (Art. 734 a 742) e Transporte de Coisas (Art. 743 a 756); (ii) Lei nº 11.442, de 05 de janeiro de 2007 - Dispõe sobre o transporte rodoviário de cargas por conta de terceiros e mediante remuneração e revoga a Lei nº 6813, de 10 de julho de 1980; (iii) Responsabilidade Civil por Roubo de Cargas; (iv) Seguro da Carga - Obrigatório e Facultativo para o Transportador; (v) Seguro da Frota de Responsabilidade Civil contra Terceiros - Danos Corporais, Materiais e Morais - Abrangência das Coberturas</span>','','0','',1,NULL,NULL,NULL,52,'0001-01-01 00:00:00','',''),(65,'','Direito do Trabalho','direito-do-trabalho','T','ASD','','0','',1,NULL,NULL,NULL,52,'0001-01-01 00:00:00','',''),(66,'','','4','T','Fundado em setembro de 2013, Carlos Jung Advogados é um escritório dedicado ao exercício da advocacia empresarial. \r\n\r\nSediado em Porto Alegre, capital do Estado do Rio Grande do Sul, o escritório esta localizado no \"polo do judiciário\", próximo dos foros e tribunais da justiça do trabalho, federal e estadual.\r\n\r\nTambém foi estruturada rede de escritórios parceiros para permitir a atuação nos demais estados da federação. A equipe de sócios e advogados é constituída por profissionais formados nas melhores universidades gaúchas e todos possuem sólida formação e experiência que permitem emitir pareceres e patrocinar causas nas diversas áreas que compõem o direito empresarial, sobretudo em favor de empresas dedicadas ao transporte rodoviário e aéreo de cargas, indústria moveleira, química e tabageira, além dos segmentos lojista e varejista.','','0','',1,NULL,NULL,NULL,53,'0001-01-01 00:00:00','',''),(67,'','','1','T','TESET','','0','',1,NULL,NULL,NULL,54,'0001-01-01 00:00:00','','');
/*!40000 ALTER TABLE `gl_dadovo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gl_enderecovo`
--

DROP TABLE IF EXISTS `gl_enderecovo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gl_enderecovo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `numero` int(11) DEFAULT NULL,
  `bairro` text,
  `endereco` text,
  `complemento` text,
  `CEP` text,
  `idCidadeVO` int(11) DEFAULT NULL,
  `idEstadoVO` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idCidadeVO` (`idCidadeVO`),
  KEY `idEstadoVO` (`idEstadoVO`),
  KEY `idCidadeVO_2` (`idCidadeVO`),
  KEY `idEstadoVO_2` (`idEstadoVO`),
  CONSTRAINT `FK7F4B6A78AC63FB69` FOREIGN KEY (`idEstadoVO`) REFERENCES `gl_estadovo` (`id`),
  CONSTRAINT `FK7F4B6A78BF87AEC5` FOREIGN KEY (`idCidadeVO`) REFERENCES `gl_cidadevo` (`id`),
  CONSTRAINT `FKFF6362E0AC63FB69` FOREIGN KEY (`idEstadoVO`) REFERENCES `gl_estadovo` (`id`),
  CONSTRAINT `FKFF6362E0BF87AEC5` FOREIGN KEY (`idCidadeVO`) REFERENCES `gl_cidadevo` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gl_enderecovo`
--

LOCK TABLES `gl_enderecovo` WRITE;
/*!40000 ALTER TABLE `gl_enderecovo` DISABLE KEYS */;
/*!40000 ALTER TABLE `gl_enderecovo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gl_estadovo`
--

DROP TABLE IF EXISTS `gl_estadovo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gl_estadovo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` text,
  `sigla` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gl_estadovo`
--

LOCK TABLES `gl_estadovo` WRITE;
/*!40000 ALTER TABLE `gl_estadovo` DISABLE KEYS */;
/*!40000 ALTER TABLE `gl_estadovo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gl_grupodepaginasvo`
--

DROP TABLE IF EXISTS `gl_grupodepaginasvo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gl_grupodepaginasvo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` text,
  `ordem` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gl_grupodepaginasvo`
--

LOCK TABLES `gl_grupodepaginasvo` WRITE;
/*!40000 ALTER TABLE `gl_grupodepaginasvo` DISABLE KEYS */;
INSERT INTO `gl_grupodepaginasvo` VALUES (1,'Controle de Páginas Fixas',999),(2,'Administração',6),(3,'Site',1),(5,'Configurações',5),(8,'Redes Sociais',4),(9,'Produtos',3),(10,'Equipe',7),(11,'Quem Somos',2);
/*!40000 ALTER TABLE `gl_grupodepaginasvo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gl_imagemcategoriavo`
--

DROP TABLE IF EXISTS `gl_imagemcategoriavo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gl_imagemcategoriavo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` text,
  `ordem` int(11) DEFAULT NULL,
  `idCategoriaVO` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idCategoriaVO` (`idCategoriaVO`),
  KEY `idCategoriaVO_2` (`idCategoriaVO`),
  CONSTRAINT `FK917123D6AEED9408` FOREIGN KEY (`idCategoriaVO`) REFERENCES `gl_categoriavo` (`id`),
  CONSTRAINT `FKFB82D532AEED9408` FOREIGN KEY (`idCategoriaVO`) REFERENCES `gl_categoriavo` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gl_imagemcategoriavo`
--

LOCK TABLES `gl_imagemcategoriavo` WRITE;
/*!40000 ALTER TABLE `gl_imagemcategoriavo` DISABLE KEYS */;
/*!40000 ALTER TABLE `gl_imagemcategoriavo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gl_imagemdadovo`
--

DROP TABLE IF EXISTS `gl_imagemdadovo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gl_imagemdadovo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` text,
  `ordem` int(11) DEFAULT NULL,
  `idDadoVO` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idDadoVO` (`idDadoVO`),
  KEY `idDadoVO_2` (`idDadoVO`),
  CONSTRAINT `FK66AFE80273F8DDDC` FOREIGN KEY (`idDadoVO`) REFERENCES `gl_dadovo` (`id`),
  CONSTRAINT `FKA14CB57C73F8DDDC` FOREIGN KEY (`idDadoVO`) REFERENCES `gl_dadovo` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=96 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gl_imagemdadovo`
--

LOCK TABLES `gl_imagemdadovo` WRITE;
/*!40000 ALTER TABLE `gl_imagemdadovo` DISABLE KEYS */;
INSERT INTO `gl_imagemdadovo` VALUES (63,'37-sobre-a-empresa-63.jpg',0,37),(64,'55--64.png',0,55),(65,'55--65.jpg',0,55),(66,'55--66.jpg',0,55),(67,'55--67.jpg',0,55),(68,'55--68.png',0,55),(69,'56-0-69.jpg',0,56),(70,'56-0-70.jpg',0,56),(71,'56-0-71.jpg',0,56),(72,'56-0-72.jpg',0,56),(73,'56-0-73.jpg',0,56),(74,'56-0-74.jpg',0,56),(75,'57-0-75.jpg',0,57),(76,'57-0-76.jpg',0,57),(77,'57-0-77.jpg',0,57),(78,'57-0-78.jpg',0,57),(79,'57-0-79.jpg',0,57),(80,'57-0-80.jpg',0,57),(83,'59-quem-somos-83.jpg',0,59),(84,'60-nossos-produtos-84.jpg',0,60),(85,'61-servicos-85.jpg',0,61),(86,'13-1-86.jpg',0,13),(88,'63-carlos-emilio-jung-88.jpg',0,63),(89,'66--89.jpg',0,66),(90,'66--90.jpg',0,66),(91,'66--91.jpg',0,66),(92,'66--92.jpg',0,66),(93,'58-fabricamos-camisas-93.jpg',0,58),(95,'62-noticias-1-95.jpg',0,62);
/*!40000 ALTER TABLE `gl_imagemdadovo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gl_imagempostvo`
--

DROP TABLE IF EXISTS `gl_imagempostvo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gl_imagempostvo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` text,
  `ordem` int(11) DEFAULT NULL,
  `idPostVO` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idPostVO` (`idPostVO`),
  CONSTRAINT `FKCB74F7F1825BB140` FOREIGN KEY (`idPostVO`) REFERENCES `gl_postvo` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gl_imagempostvo`
--

LOCK TABLES `gl_imagempostvo` WRITE;
/*!40000 ALTER TABLE `gl_imagempostvo` DISABLE KEYS */;
/*!40000 ALTER TABLE `gl_imagempostvo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gl_imagemsegfilhovo`
--

DROP TABLE IF EXISTS `gl_imagemsegfilhovo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gl_imagemsegfilhovo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` text,
  `ordem` int(11) DEFAULT NULL,
  `idSegmentoFilhoVO` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idSegmentoFilhoVO` (`idSegmentoFilhoVO`),
  KEY `idSegmentoFilhoVO_2` (`idSegmentoFilhoVO`),
  CONSTRAINT `FK53A55A0674AF9F36` FOREIGN KEY (`idSegmentoFilhoVO`) REFERENCES `gl_segmentofilhovo` (`id`),
  CONSTRAINT `FKCEED822174AF9F36` FOREIGN KEY (`idSegmentoFilhoVO`) REFERENCES `gl_segmentofilhovo` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gl_imagemsegfilhovo`
--

LOCK TABLES `gl_imagemsegfilhovo` WRITE;
/*!40000 ALTER TABLE `gl_imagemsegfilhovo` DISABLE KEYS */;
/*!40000 ALTER TABLE `gl_imagemsegfilhovo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gl_imagemsegpaivo`
--

DROP TABLE IF EXISTS `gl_imagemsegpaivo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gl_imagemsegpaivo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` text,
  `ordem` int(11) DEFAULT NULL,
  `idSegmentoPaiVO` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idSegmentoPaiVO` (`idSegmentoPaiVO`),
  KEY `idSegmentoPaiVO_2` (`idSegmentoPaiVO`),
  CONSTRAINT `FK7972E59EDE0F7C3F` FOREIGN KEY (`idSegmentoPaiVO`) REFERENCES `gl_segmentopaivo` (`id`),
  CONSTRAINT `FKCAF099E4DE0F7C3F` FOREIGN KEY (`idSegmentoPaiVO`) REFERENCES `gl_segmentopaivo` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gl_imagemsegpaivo`
--

LOCK TABLES `gl_imagemsegpaivo` WRITE;
/*!40000 ALTER TABLE `gl_imagemsegpaivo` DISABLE KEYS */;
/*!40000 ALTER TABLE `gl_imagemsegpaivo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gl_paginadecontrolevo`
--

DROP TABLE IF EXISTS `gl_paginadecontrolevo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gl_paginadecontrolevo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` text,
  `pagina` text,
  `ordem` int(11) DEFAULT NULL,
  `fixa` tinyint(1) DEFAULT NULL,
  `construcao` tinyint(1) DEFAULT NULL,
  `idGrupoDePaginasVO` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idGrupoDePaginasVO` (`idGrupoDePaginasVO`),
  KEY `idGrupoDePaginasVO_2` (`idGrupoDePaginasVO`),
  CONSTRAINT `FK89693901FD33DDA` FOREIGN KEY (`idGrupoDePaginasVO`) REFERENCES `gl_grupodepaginasvo` (`id`),
  CONSTRAINT `FKD68C58EEFD33DDA` FOREIGN KEY (`idGrupoDePaginasVO`) REFERENCES `gl_grupodepaginasvo` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gl_paginadecontrolevo`
--

LOCK TABLES `gl_paginadecontrolevo` WRITE;
/*!40000 ALTER TABLE `gl_paginadecontrolevo` DISABLE KEYS */;
INSERT INTO `gl_paginadecontrolevo` VALUES (1,'Telas Personalizadas','/Controle/Cadastro/ControlePaginas/Paginas.aspx',0,1,1,1),(2,'Tela de Seg. Pai','/Controle/Cadastro/ControlePaginas/PaginaSegPai.aspx',0,1,1,1),(3,'Tela de Seg. Filho','/Controle/Cadastro/ControlePaginas/PaginaSegFilho.aspx',0,1,1,1),(4,'Tela de Categoria','/Controle/Cadastro/ControlePaginas/PaginaCategoria.aspx',0,1,1,1),(6,'Demais Telas','/Controle/Cadastro/ControlePaginas/PaginaDemais.aspx',0,1,1,1),(7,'Organizador','/Controle/Cadastro/ControlePaginas/Organizador.aspx',0,1,1,1),(9,'Usuários','/Controle/Cadastro/Usuarios.aspx',1,1,0,2),(10,'Controle de Acesso','/Controle/Cadastro/ControleAcesso.aspx',2,1,0,2),(14,'Rodapé','/Controle/Cadastro/3/14',9,0,0,3),(20,'Contato','/Controle/Cadastro/3/20',7,0,0,3),(22,'Analytics','/Controle/Cadastro/5/22',1,0,0,5),(23,'MetaTags Principal','/Controle/Cadastro/5/23',2,0,0,5),(24,'Configurações de SMTP','/Controle/Cadastro/5/24',5,0,0,5),(26,'Controle de Sitemap','/Controle/Cadastro/ControleSitemap.aspx',4,1,0,5),(35,'Quem Somos','/Controle/Cadastro/3/35',5,0,0,3),(36,'Cabeçalho','/Controle/Cadastro/3/36',8,0,0,3),(51,'MetaTags por Página','/Controle/Cadastro/5/51',3,0,0,5),(57,'Facebook','/Controle/Cadastro/8/57',1,0,0,8),(58,'Twitter','/Controle/Cadastro/8/58',4,0,0,NULL),(59,'Instagram','/Controle/Cadastro/8/59',2,0,0,8),(60,'Youtube','/Controle/Cadastro/8/60',3,0,0,8),(61,'Animação Home','/Controle/Cadastro/3/61',1,0,0,3),(62,'Serviços','/Controle/Cadastro/3/62',6,0,0,3),(63,'Produtos','/Controle/Cadastro/9/63',2,0,0,9),(64,'Segmento','/Controle/Cadastro/SegmentosPai.aspx',1,1,0,9),(65,'Menus Home','/Controle/Cadastro/3/65',2,0,0,3),(66,'Notícias','/Controle/Cadastro/3/66',3,0,0,3),(67,'Membros','/Controle/Cadastro/10/67',2,0,0,10),(68,'Texto','/Controle/Cadastro/10/68',1,0,0,10),(69,'Áreas de Atuação','/Controle/Cadastro/3/69',4,0,0,3),(70,'Nossa História','/Controle/Cadastro/11/70',1,0,0,11),(71,'Missão e Valores','/Controle/Cadastro/11/71',2,0,0,11),(72,'Parceiros','/Controle/Cadastro/11/72',3,0,0,11),(73,'Advocacia de Apoio','/Controle/Cadastro/11/73',4,0,0,11);
/*!40000 ALTER TABLE `gl_paginadecontrolevo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gl_permissaogrupodepaginasvo`
--

DROP TABLE IF EXISTS `gl_permissaogrupodepaginasvo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gl_permissaogrupodepaginasvo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idGrupoDePaginasVO` int(11) DEFAULT NULL,
  `idUsuarioVO` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idGrupoDePaginasVO` (`idGrupoDePaginasVO`),
  KEY `idUsuarioVO` (`idUsuarioVO`),
  KEY `idGrupoDePaginasVO_2` (`idGrupoDePaginasVO`),
  KEY `idUsuarioVO_2` (`idUsuarioVO`),
  CONSTRAINT `FK3279D823F2BB2D60` FOREIGN KEY (`idUsuarioVO`) REFERENCES `gl_usuariovo` (`id`),
  CONSTRAINT `FK3279D823FD33DDA` FOREIGN KEY (`idGrupoDePaginasVO`) REFERENCES `gl_grupodepaginasvo` (`id`),
  CONSTRAINT `FK550C8DDF2BB2D60` FOREIGN KEY (`idUsuarioVO`) REFERENCES `gl_usuariovo` (`id`),
  CONSTRAINT `FK550C8DDFD33DDA` FOREIGN KEY (`idGrupoDePaginasVO`) REFERENCES `gl_grupodepaginasvo` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gl_permissaogrupodepaginasvo`
--

LOCK TABLES `gl_permissaogrupodepaginasvo` WRITE;
/*!40000 ALTER TABLE `gl_permissaogrupodepaginasvo` DISABLE KEYS */;
/*!40000 ALTER TABLE `gl_permissaogrupodepaginasvo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gl_permissaovo`
--

DROP TABLE IF EXISTS `gl_permissaovo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gl_permissaovo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idPaginaDeControleVO` int(11) DEFAULT NULL,
  `idUsuarioVO` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idPaginaDeControleVO` (`idPaginaDeControleVO`),
  KEY `idUsuarioVO` (`idUsuarioVO`),
  KEY `idPaginaDeControleVO_2` (`idPaginaDeControleVO`),
  KEY `idUsuarioVO_2` (`idUsuarioVO`),
  CONSTRAINT `FKF0D62BBB24F53D72` FOREIGN KEY (`idPaginaDeControleVO`) REFERENCES `gl_paginadecontrolevo` (`id`),
  CONSTRAINT `FKF0D62BBBF2BB2D60` FOREIGN KEY (`idUsuarioVO`) REFERENCES `gl_usuariovo` (`id`),
  CONSTRAINT `FKFD46AFAA24F53D72` FOREIGN KEY (`idPaginaDeControleVO`) REFERENCES `gl_paginadecontrolevo` (`id`),
  CONSTRAINT `FKFD46AFAAF2BB2D60` FOREIGN KEY (`idUsuarioVO`) REFERENCES `gl_usuariovo` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gl_permissaovo`
--

LOCK TABLES `gl_permissaovo` WRITE;
/*!40000 ALTER TABLE `gl_permissaovo` DISABLE KEYS */;
/*!40000 ALTER TABLE `gl_permissaovo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gl_postvo`
--

DROP TABLE IF EXISTS `gl_postvo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gl_postvo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` text,
  `chave` text,
  `data` datetime DEFAULT NULL,
  `texto` text,
  `imagem` text,
  `visivel` tinyint(1) DEFAULT NULL,
  `tipo` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gl_postvo`
--

LOCK TABLES `gl_postvo` WRITE;
/*!40000 ALTER TABLE `gl_postvo` DISABLE KEYS */;
/*!40000 ALTER TABLE `gl_postvo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gl_segmentofilhovo`
--

DROP TABLE IF EXISTS `gl_segmentofilhovo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gl_segmentofilhovo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` text,
  `chave` text,
  `descricao` text,
  `ordem` text,
  `visivel` tinyint(1) DEFAULT NULL,
  `idTela` int(11) DEFAULT NULL,
  `idSegmentoPaiVO` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idTela` (`idTela`),
  KEY `idSegmentoPaiVO` (`idSegmentoPaiVO`),
  KEY `idTela_2` (`idTela`),
  KEY `idSegmentoPaiVO_2` (`idSegmentoPaiVO`),
  CONSTRAINT `FKAEB64D4773E1DFF6` FOREIGN KEY (`idTela`) REFERENCES `gl_tela` (`id`),
  CONSTRAINT `FKAEB64D47DE0F7C3F` FOREIGN KEY (`idSegmentoPaiVO`) REFERENCES `gl_segmentopaivo` (`id`),
  CONSTRAINT `FKC375057073E1DFF6` FOREIGN KEY (`idTela`) REFERENCES `gl_tela` (`id`),
  CONSTRAINT `FKC3750570DE0F7C3F` FOREIGN KEY (`idSegmentoPaiVO`) REFERENCES `gl_segmentopaivo` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gl_segmentofilhovo`
--

LOCK TABLES `gl_segmentofilhovo` WRITE;
/*!40000 ALTER TABLE `gl_segmentofilhovo` DISABLE KEYS */;
/*!40000 ALTER TABLE `gl_segmentofilhovo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gl_segmentopaivo`
--

DROP TABLE IF EXISTS `gl_segmentopaivo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gl_segmentopaivo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` text,
  `chave` text,
  `visivel` tinyint(1) DEFAULT NULL,
  `descricao` text,
  `ordem` text,
  `idTela` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idTela` (`idTela`),
  KEY `idTela_2` (`idTela`),
  CONSTRAINT `FK3D114DDB73E1DFF6` FOREIGN KEY (`idTela`) REFERENCES `gl_tela` (`id`),
  CONSTRAINT `FK62D1119673E1DFF6` FOREIGN KEY (`idTela`) REFERENCES `gl_tela` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gl_segmentopaivo`
--

LOCK TABLES `gl_segmentopaivo` WRITE;
/*!40000 ALTER TABLE `gl_segmentopaivo` DISABLE KEYS */;
INSERT INTO `gl_segmentopaivo` VALUES (8,'Camisas Masculinas','camisas-masculinas',1,'','1',47),(9,'Camisas Femininas','camisas-femininas',1,'','2',47),(10,'Uniformes','uniformes',1,'','3',47);
/*!40000 ALTER TABLE `gl_segmentopaivo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gl_tela`
--

DROP TABLE IF EXISTS `gl_tela`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gl_tela` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` text,
  `nomeFixo` text,
  `multiplo` tinyint(1) DEFAULT NULL,
  `idUploadTela` int(11) DEFAULT NULL,
  `idPaginaDeControleVO` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idUploadTela` (`idUploadTela`),
  KEY `idPaginaDeControleVO` (`idPaginaDeControleVO`),
  KEY `idUploadTela_2` (`idUploadTela`),
  KEY `idPaginaDeControleVO_2` (`idPaginaDeControleVO`),
  CONSTRAINT `FK2E000F3E24F53D72` FOREIGN KEY (`idPaginaDeControleVO`) REFERENCES `gl_paginadecontrolevo` (`id`),
  CONSTRAINT `FK2E000F3EDC8DB668` FOREIGN KEY (`idUploadTela`) REFERENCES `gl_uploadtela` (`id`),
  CONSTRAINT `FK3280131E24F53D72` FOREIGN KEY (`idPaginaDeControleVO`) REFERENCES `gl_paginadecontrolevo` (`id`),
  CONSTRAINT `FK3280131EDC8DB668` FOREIGN KEY (`idUploadTela`) REFERENCES `gl_uploadtela` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gl_tela`
--

LOCK TABLES `gl_tela` WRITE;
/*!40000 ALTER TABLE `gl_tela` DISABLE KEYS */;
INSERT INTO `gl_tela` VALUES (5,'Rodapé',NULL,0,NULL,14),(11,'Contato',NULL,0,26,20),(13,'Analytics',NULL,0,NULL,22),(14,'MetaTags Principal',NULL,0,NULL,23),(15,'Configurações de SMTP',NULL,0,NULL,24),(16,'Barra GetSocial',NULL,0,NULL,NULL),(23,'Segmento','SegmentoPai',0,NULL,64),(24,'Segmento Filho','SegmentoFilho',0,NULL,NULL),(25,'Quem Somos',NULL,0,9,35),(26,'Cabeçalho',NULL,0,NULL,36),(29,'Categoria','Categoria',0,NULL,NULL),(35,'MetaTags por Página',NULL,1,NULL,51),(41,'Facebook',NULL,0,NULL,57),(42,'Twitter',NULL,0,NULL,58),(43,'Instagram',NULL,0,NULL,59),(44,'Youtube',NULL,0,NULL,60),(45,'Animação Home',NULL,0,22,61),(46,'Serviços',NULL,0,23,62),(47,'Produtos',NULL,1,24,63),(48,'Menus Home',NULL,1,25,65),(49,'Notícias',NULL,1,27,66),(50,'Membros',NULL,1,28,67),(51,'Texto',NULL,0,NULL,68),(52,'Áreas de Atuação',NULL,1,NULL,69),(53,'Nossa História',NULL,0,29,70),(54,'Missão e Valores',NULL,0,30,71),(55,'Parceiros',NULL,0,31,72),(56,'Advocacia de Apoio',NULL,0,32,73);
/*!40000 ALTER TABLE `gl_tela` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gl_uploadtela`
--

DROP TABLE IF EXISTS `gl_uploadtela`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gl_uploadtela` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `QtdeFotos` int(11) DEFAULT NULL,
  `TamFotoGrW` int(11) DEFAULT NULL,
  `TamFotoPqW` int(11) DEFAULT NULL,
  `TamFotoGrH` int(11) DEFAULT NULL,
  `TamFotoPqH` int(11) DEFAULT NULL,
  `Configuracao` int(11) DEFAULT NULL,
  `Qualidade` int(11) DEFAULT NULL,
  `Cor` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gl_uploadtela`
--

LOCK TABLES `gl_uploadtela` WRITE;
/*!40000 ALTER TABLE `gl_uploadtela` DISABLE KEYS */;
INSERT INTO `gl_uploadtela` VALUES (9,1,470,470,340,340,2,100,NULL),(12,15,540,200,540,200,2,80,NULL),(22,15,1170,1170,400,400,2,90,NULL),(23,20,800,400,600,300,2,80,NULL),(24,12,800,400,600,300,2,80,NULL),(25,1,364,364,194,194,2,80,NULL),(26,1,1920,1920,900,900,2,100,NULL),(27,1,445,262,445,263,1,80,NULL),(28,1,347,262,288,218,2,90,NULL),(29,8,252,252,252,252,2,90,NULL),(30,8,262,262,262,262,2,90,NULL),(31,8,262,262,262,262,2,90,NULL),(32,8,262,262,262,262,2,90,NULL);
/*!40000 ALTER TABLE `gl_uploadtela` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gl_usuariovo`
--

DROP TABLE IF EXISTS `gl_usuariovo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gl_usuariovo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` text,
  `login` text,
  `senha` text,
  `tipo` text,
  `status` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gl_usuariovo`
--

LOCK TABLES `gl_usuariovo` WRITE;
/*!40000 ALTER TABLE `gl_usuariovo` DISABLE KEYS */;
INSERT INTO `gl_usuariovo` VALUES (1,'admin','admin','57e9557fe73a6b4b7aa97cd3752ae51607d0c179','AA','AT');
/*!40000 ALTER TABLE `gl_usuariovo` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-21  8:06:21
