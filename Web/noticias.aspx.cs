﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Linq;
using System.Collections.Generic;
using Modelos;

public partial class _noticias : System.Web.UI.Page
{  
   protected void Page_Load(object sender, EventArgs e)
   {

      Page.Title = "Notícias - " + Configuracoes.getSetting("NomeSite");

      if (!IsPostBack)
      {
       
         IList<DadoVO> dadoNoticias = null;
         dadoNoticias = MetodosFE.documentos.Where(x => x.tela.nome.Equals("Notícias") && x.visivel).OrderBy(x => x.data).ToList();

         if (dadoNoticias != null && dadoNoticias.Count > 0)
         {
            repNoticias.DataSource = dadoNoticias;
            repNoticias.DataBind();
         }
      }
   }
}
