﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="equipe.aspx.cs" Inherits="_equipe" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


      
  <!--=== CONTEÚDO ===-->
        <div class="fundo-aequipe"></div>

        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 aequipe">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 aequipe-texto font-robotolight">
                        <asp:Literal runat="server" ID="litTexto"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="container content">

            <div class="row">
                <!-- Team v2 -->     
               <asp:Repeater runat="server" ID="repEquipe">
                <ItemTemplate>
                <div class="col-md-3 col-sm-6">
                    <div class="team-v2">
                        <img class="img-responsive" src="<%# ((Modelos.DadoVO)Container.DataItem).getPrimeiraImagemLQ() %>" alt="">
                        <div class="inner-team">
                            <h3 class="font-robotoregular"><%# ((Modelos.DadoVO)Container.DataItem).nome %></h3>
                            <i class="color-blue font-robotoregular"><%# ((Modelos.DadoVO)Container.DataItem).referencia  %></i>
                            <div class="dados font-robotoregular">
                                <%# ((Modelos.DadoVO)Container.DataItem).descricao %>
                            </div>
                            <div class="modal-botao">
                                <!-- Large modal -->
                                <a data-toggle="modal" class="rounded" data-target="#myModal<%# ((Modelos.DadoVO)Container.DataItem).id %>">Saiba mais</a>

                                <div class="modal fade" id="myModal<%# ((Modelos.DadoVO)Container.DataItem).id %>"  tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 aria-hidden="true" data-dismiss="modal" class="close" type="button">
                                                    VOLTAR
                                                </h4>

                                            </div>
                                            <div class="modal-body font-robotolight">
                                                <img src="<%# ((Modelos.DadoVO)Container.DataItem).getPrimeiraImagemHQ() %>" class="img-responsive" />
                                                <h2 class="font-arial-regular">
                                                    <%# ((Modelos.DadoVO)Container.DataItem).nome %>
                                                </h2>
                                                <h3 class="font-arial-regular">
                                                    <i><%# ((Modelos.DadoVO)Container.DataItem).referencia %></i>
                                                </h3>
                                                <%# ((Modelos.DadoVO)Container.DataItem).descricao %>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Large modal -->
                            </div>
                        </div>
                    </div>
                </div>
            </ItemTemplate>
         </asp:Repeater>

                <!-- End Team v2 -->


            </div>
        </div>
        <!--=== FIM CONTEÚDO ===-->




</asp:Content>
