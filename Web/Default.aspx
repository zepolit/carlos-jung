﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Default.aspx.cs" Inherits="_Default" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


       <div class="header-xs hidden-lg hidden-md hidden-sm ">

    </div>
        <!--=== CONTEÚDO ===-->
     
        <!--=== Slider ===-->
        <div class="slide  hidden-xs">
            <div class="tp-banner-container">
                <div class="tp-banner">
                    <ul>
                     <asp:Repeater runat="server" ID="repImagens">
                        <ItemTemplate>
                        <!-- SLIDE -->
                        <li class="thumbimage" data-transition="prandom-premium" data-slotamount="5" data-masterspeed="1000" data-title="Slide 1">
                            <!-- MAIN IMAGE -->
                            <img src="<%# ((Modelos.ImagemDadoVO)Container.DataItem).getEnderecoImagemHQ() %>"" alt="darkblurbg" data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">

                            <div class="p-bannertimer revolution-ch1 sft start font-myriadpro-regular"
                                 data-x="center"
                                 data-hoffset="0"
                                 data-y="160"
                                 data-speed="1500"
                                 data-start="500"
                                 data-easing="Back.easeInOut"
                                 data-endeasing="Power1.easeIn"
                                 data-endspeed="300">
                            </div>
                        </li>
                        <!-- END SLIDE -->  
                           </ItemTemplate>
                      </asp:Repeater>                   
                      
                    </ul>
                    <div class="tp-bannertimer tp-bottom"></div>
                </div>
            </div>
        </div>
        <!--=== End Slider ===-->
        <!--===  HOME1 ===-->
        <div class="container content home1">

            <div class="row">
                <div class="headline-center-v2 margin-bottom-20 margin-top-20 ">                    
                    <span class="bordered-icon font-robotolight">Notícias</span>                   
                </div>
               <asp:Repeater runat="server" ID="repNoticias">
               <ItemTemplate>
                <!-- Begin Easy Block -->
                <div class="col-lg-3 col-md-3 col-sm-6 home-noticias">
                    <div class="easy-block-v1">
                        <div class="easy-block-v1-badge rgba-default">     <%# ((Modelos.DadoVO)Container.DataItem).data.ToShortDateString() %></div>
                        <img class="img-responsive" alt="" src="<%# ((Modelos.DadoVO)Container.DataItem).getPrimeiraImagemLQ() %>">
                        <div class="home-noticias-texto">
                            <%# ((Modelos.DadoVO)Container.DataItem).descricao %>
                        </div>
                        <a href="<%# String.Format("{0}/noticia/{1}", MetodosFE.BaseURL, ((Modelos.DadoVO)Container.DataItem).chave) %>" class="pull-right font-arial-bold">VER MAIS</a>
                    </div>
                </div>
                <!-- End Begin Easy Block -->
              </ItemTemplate>
            </asp:Repeater>
            </div>

        </div>
        <!--=== FIM HOME1 ===-->
        <!--=== HOME2 ===-->
    <div class="home2">
        <div class="container content-sm">
            <div class="row">
                <asp:Repeater runat="server" ID="repMenusHome">
                    <ItemTemplate>
                <div class="col-sm-4">
                    <div class="thumbnails-v1">
                        <div class="thumbnail-img">
                            <img class="img-responsive" src="<%# ((Modelos.DadoVO)Container.DataItem).getPrimeiraImagemLQ() %>" alt="">
                        </div>
                        <div class="caption">
                            <h3 class="font-robotolight"><%# ((Modelos.DadoVO)Container.DataItem).nome %></h3>
                            <h4 class="font-robotoregular"><%# ((Modelos.DadoVO)Container.DataItem).referencia %></h4>
                            <div class="texto-home2 dotdotMenuHome">
                                <%# ((Modelos.DadoVO)Container.DataItem).descricao %>
                            </div>
                            <a class="btn-u btn-brd rounded btn-u-green btn-u-sm font-robotoregular" href="<%# String.Format("{0}/{1}" , MetodosFE.BaseURL, ((Modelos.DadoVO)Container.DataItem).valor) %>">VER  MAIS</a>
                            
                        </div>
                    </div>
                </div>
                </ItemTemplate>
                    </asp:Repeater>
               
            </div>
        </div>
    </div>
        <!--===  FIM HOME2 ===-->


        <!--=== HOME3 ===-->
        <div class="bg-image-v1 parallaxBg" style="background-position: 50% 79px;">
            <div class="container">
                <div class="headline-center headline-light">
                    <h2 class="font-robotolight"><asp:Literal runat="server" ID="litTituloChamadaComercial"/></h2>
                   <div class="contatenos font-robotolight">
                       <asp:Literal runat="server" ID="litTextoChamadaComercial"/>
                   </div>
                    <a class="btn-u rounded contatenos-botao font-robotoregular" href="<%# MetodosFE.BaseURL%>/Contato">faça contato</a>
                </div><!--/end Headline Center-->
            </div>
        </div>
        <!--===  FIM HOME3 ===-->
        <!--=== FIM CONTEÚDO ===-->





</asp:Content>
