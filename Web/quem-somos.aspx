﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="quem-somos.aspx.cs" Inherits="_quemSomos" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


   <div class="backgound-quemsomos">
 <div class="header" style="margin-top:118px;">
    <div class="sub-menu hidden-sm hidden-xs">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 sub-menu-12">
                            <ul>
                                <li runat="server" id="liNossaHistoria" class="">
                                    <asp:LinkButton runat="server" ID="btnNossaHistoria" OnClick="linkNossaHistoria_Click">
                                        NOSSA HISTÓRIA
                                    </asp:LinkButton>
                                </li>
                                <li runat="server" id="limissao" class="">
                                     <asp:LinkButton runat="server" ID="btnMissao" OnClick="btnMissao_Click">
                                        MISSÃO E VALORES 
                                     </asp:LinkButton>
                                </li>
                                <li runat="server" id="liParceiros" class="">
                                    <asp:LinkButton runat="server" ID="btnParceiros" OnClick="btnParceiros_Click">
                                        PARCEIROS  
                                    </asp:LinkButton>
                                </li>
                                <li runat="server" id="liAdvocacia" class="">
                                   <asp:LinkButton runat="server" ID="btnAdvocacia" OnClick="btnAdvocacia_Click">
                                        ADVOCACIA DE APOIO
                                     </asp:LinkButton>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Navbar -->
      </div>
    <!--=== CONTEÚDO ===-->
        <div class="quemsomos">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 quemsomos-topo">   
                        <div class="menu-conteudo hidden-lg hidden-md ">
                            <ul>
                                <li class="active">
                                    <a href="#">
                                        NOSSA HISTÓRIA
                                    </a>
                                </li>
                                <li class="">
                                    <a href="#">
                                        MISSÃO E VALORES
                                    </a>
                                </li>
                                <li class="">
                                    <a href="#">
                                        PARCEIROS
                                    </a>
                                </li>
                                <li class="">
                                    <a href="#">
                                        ADVOCACIA DE APOIO
                                    </a>
                                </li>
                            </ul>
                        </div>                     
                        <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 quemsomos-texto font-robotoregular">
                            <asp:Literal runat="server" ID="litTexto"/>
                        </div>
                        <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">

                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 quemsomos-baixo">
                     <asp:Repeater runat="server" ID="repImgs" >
                        <Itemtemplate>

                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 quemsomos-img">
                            <img src="<%# ((Modelos.ImagemDadoVO)Container.DataItem).getEnderecoImagemHQ() %>" class="img-responsive" />
                        </div>

                       </Itemtemplate>
                     </asp:Repeater>
                    </div>
                </div>
            </div>
        </div>
        <!--=== FIM CONTEÚDO ===-->
       

</div>
</asp:Content>
