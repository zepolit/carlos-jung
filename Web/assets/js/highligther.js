﻿$(document).ready(function () {

    var url = window.location.pathname;

    while (url) {
        //$( ".resultados" ).append(url + " - O Link<br/>");

        var quantidade = quantidadeDeLinks(url);
        //      $( ".resultados" ).append(quantidade + "<br/>");
        if (quantidade > 1) {
            $('ul a[href="' + url + '"]').each(function () {
                    $(this).parent().addClass("active");
            });
        }
        else if (quantidade == 1) {
            $('[href*="' + url + '"]').each(function () {
                    $(this).parent().addClass("active");
            });
        }
        url = cortaURL(url);
    };


});

function cortaURL(url) {
    var index = url.lastIndexOf('/');

    var x = url.substring(0, index);
    return x;
}

function quantidadeDeLinks(link) {
    var contagem = 0;

    contagem = $('[href*="' + link + '"]').length;
    return contagem;
};

