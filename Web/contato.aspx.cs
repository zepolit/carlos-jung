﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Drawing;
using System.Collections.Generic;
using Modelos;

public partial class _contato : System.Web.UI.Page
{
   protected void Page_Load(object sender, EventArgs e)
   {
      DadoVO dadoImgBackGroundContato = null;
      dadoImgBackGroundContato = MetodosFE.getTela("Contato");
      string urlImgBackGround = dadoImgBackGroundContato.getPrimeiraImagemHQ();
      backgoundContato.Attributes["Style"] = "background-repeat: no - repeat; background-position: center; background-color: #355e9a; background-attachment: fixed; background-image: url" + "(" + urlImgBackGround + ")";

      MaintainScrollPositionOnPostBack = true;
      if (!IsPostBack)
      {
         Page.Title = "Contato - " + Configuracoes.getSetting("NomeSite");

         DadoVO dado = MetodosFE.getTela("Contato");

         if (dado != null)
         {
            litEndereco.Text = dado.descricao;
            litTelefones.Text = dado.nome;
            litEmail.Text = dado.referencia;
         }
      }


   }
   protected void Button1_Click(object sender, EventArgs e)
   {
      litErro.Text = "";
      litSucesso.Text = "";
      try
      {
         if (String.IsNullOrEmpty(txtnome.Text) || !ControleValidacao.validaEmail(txtEmail.Text))
            throw new Exception("Campos Nome e E-mail obrigatórios.");


         EnvioEmailsVO envio = new EnvioEmailsVO();

         DadoVO dado = MetodosFE.getTela("Configurações de SMTP");

         if (dado != null)
         {
            DadoVO dadosContato = MetodosFE.getTela("Contato");
            string email = null;
            if (dadosContato != null)
               if (!String.IsNullOrEmpty(dadosContato.referencia))
                  email = dadosContato.referencia;

            if (String.IsNullOrEmpty(email))
               email = dado.referencia;

            envio.nomeRemetente = Configuracoes.getSetting("NomeSite");
            envio.emailRemetente = dado.referencia;
            envio.emailDestinatario = email;
            envio.assuntoMensagem = "Contato do Site";
            envio.emailResposta = txtEmail.Text;



            string mensagem = "";
            mensagem += "<br/>Nome: " + txtnome.Text;
            mensagem += "<br/>E-mail: " + txtEmail.Text;
            mensagem += "<br/>Telefone: " + txtFone.Text;
            mensagem += "<br/>Comentários: " + txtcomentario.Text;



            envio.conteudoMensagem = mensagem;

            bool vrecebe = EnvioEmails.envioemails(envio);

            if (vrecebe)
            {

               litSucesso.Text = "E-mail enviado com sucesso !";
               LimparCampos();
            }
            else
            {
               litErro.Text = "Ocorreram problemas no envio do e-mail. Tente mais tarde.";
            }
         }
         else
            throw new Exception("Problemas ocorreram na configuração de E-mail.");
      }
      catch (Exception ex)
      {
         litErro.Text = ex.Message;
      }

   }



   protected void LimparCampos()
   {
      try
      {
         txtcomentario.Text = "";
         txtEmail.Text = "";
         txtnome.Text = "";
         txtFone.Text = "";
         txtcomentario.Text = "";
      }
      catch (ApplicationException er)
      {
         litSucesso.Text = "";
         litErro.Text = er.Message;
      }
   }
}
