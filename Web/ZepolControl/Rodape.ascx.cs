﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Modelos;

public partial class ZepolControl_Rodape : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            DadoVO dadoSobre = null;
            dadoSobre = MetodosFE.getTela("Sobre");
            if (dadoSobre != null)
            {

                litTituloSobre.Text = dadoSobre.nome;
                litTextoSobre.Text = dadoSobre.descricao;

            }

            DadoVO dadoEndereco = null;
            dadoEndereco = MetodosFE.getTela("Endereço");
            if (dadoEndereco != null)
            {
                litTituloEndereco.Text = dadoEndereco.nome;
                litEndereco.Text = dadoEndereco.descricao;
                litTelefones.Text = dadoEndereco.valor;
                litEmail.Text = dadoEndereco.referencia;
            }

            DadoVO dadoNews = null;
            dadoNews = MetodosFE.getTela("News e Redes Sociais");
            if (dadoNews != null)
            {
                litTituloNews.Text = dadoNews.nome;
                litTextoNews.Text = dadoNews.descricao;
            }

            DadoVO dado = null;
            dado = MetodosFE.getTela("Facebook");
            if (dado != null)
            {
                if (!String.IsNullOrEmpty(dado.nome))
                    linkFacebook.HRef = dado.nome;
                else
                    linkFacebook.Visible = false;
            }

            dado = MetodosFE.getTela("Twitter");
            if (dado != null)
            {
                if (!String.IsNullOrEmpty(dado.nome))
                    linkTwitter.HRef = dado.nome;
                else
                    linkTwitter.Visible = false;
            }

            dado = MetodosFE.getTela("Linkedin");
            if (dado != null)
            {
                if (!String.IsNullOrEmpty(dado.nome))
                    linkLinkedin.HRef = dado.nome;
                else
                    linkLinkedin.Visible = false;
            }

        }
    }


    protected void lb_Cadastrar_Click(object sender, EventArgs e)
    {
        try
        {
            if (!ControleValidacao.validaEmail(txtEmail.Text))
                throw new Exception("E-mail obrigatório");


            EnvioEmailsVO envio = new EnvioEmailsVO();

            DadoVO dado = MetodosFE.getTela("Configurações de SMTP");

            if (dado != null)
            {
                DadoVO dadosContato = MetodosFE.getTela("Contato");
                string email = null;
                if (dadosContato != null)
                    if (!String.IsNullOrEmpty(dadosContato.referencia))
                        email = dadosContato.referencia;

                if (String.IsNullOrEmpty(email))
                    email = dado.referencia;

                envio.nomeRemetente = Configuracoes.getSetting("NomeSite");
                envio.emailRemetente = dado.referencia;
                envio.emailDestinatario = email;
                envio.assuntoMensagem = "Newsletter - Contato do Site";
                envio.emailResposta = txtEmail.Text;



                string mensagem = "";
                mensagem += "<br/>O cliente abaixo deseja receber a Newsletter : <br/><br/>";
                mensagem += "<br/>E-mail: " + txtEmail.Text;

                envio.conteudoMensagem = mensagem;

                bool vrecebe = EnvioEmails.envioemails(envio);

                if (vrecebe)
                {

                    litSucesso.Text = "E-mail enviado com sucesso !";
                    LimparCampos();
                }
                else
                {
                    litErro.Text = "Ocorreram problemas no envio do e-mail. Tente mais tarde.";
                }
            }
            else
                throw new Exception("Problemas ocorreram na configuração de E-mail.");
        }
        catch (Exception ex)
        {
            litErro.Text = ex.Message;
        }
    }


    protected void LimparCampos()
    {
        try
        {
            txtEmail.Text = "";
        }
        catch (ApplicationException er)
        {
            litSucesso.Text = "";
            litErro.Text = er.Message;
        }
    }
}