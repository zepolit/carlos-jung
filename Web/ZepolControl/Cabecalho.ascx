﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Cabecalho.ascx.cs" Inherits="ZepolControl_DadosTexto" %>
<!-- Controles -->

<%@ Import Namespace="Modelos" %>
    <div class="wrapper">
  <!--=== Header ===-->
        <div class="header">
            <!-- Navbar -->
            <div class="navbar navbar-default mega-menu" role="navigation">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="fa fa-bars"></span>
                        </button>
                        <a class="navbar-brand" href="index.html">
                            <img class="img-responsive" id="logo-header" src="<%# MetodosFE.BaseURL %>/assets/img/logomenu.png" alt="Logo">
                        </a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse navbar-responsive-collapse font-arial-regular">
                        <ul class="nav navbar-nav">
                            <!-- Home -->
                            <li class="">
                                <a href="<%# MetodosFE.BaseURL %>/">
                                    HOME
                                </a>
                            </li>
                            <!-- End Home -->
                            <!-- Pages -->
                            <li class="">
                                <a href="<%# MetodosFE.BaseURL %>/quem-somos">
                                    o escritório
                                </a>
                            </li>                           
                            <!-- End Pages -->
                            <!-- Features -->
                            <li class="">
                                <a href="<%# MetodosFE.BaseURL %>/areas-de-atuacao">
                                    áreas de atuação
                                </a>
                            </li>
                            <!-- End Features -->
                            <!-- Portfolio -->
                            <li class="">
                                <a href="<%# MetodosFE.BaseURL %>/equipe">
                                    a equipe
                                </a>
                            </li>
                            <!-- Ens Portfolio -->
                            <!-- Blog -->
                            <li class="">
                                <a href="<%# MetodosFE.BaseURL %>/noticias">
                                    notícias
                                </a>                                
                            </li>
                            <!-- End Blog -->
                            <!-- Contacts -->
                            <li class="">
                                <a href="<%# MetodosFE.BaseURL %>/contato">
                                    Contato
                                </a>
                            </li>
                        </ul>
                    </div><!--/navbar-collapse-->
                </div>
            </div>
            <!-- End Navbar -->
        </div>
</div>
        <!--=== End Header ===-->
