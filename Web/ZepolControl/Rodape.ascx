﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Rodape.ascx.cs" Inherits="ZepolControl_Rodape" %>

  <!--=== Footer Version 1 ===-->
        <div class="faixa-rodape"></div>
        <div class="footer-v1">
            <div class="footer">
                <div class="container">
                    <div class="row">

                        <!-- End About -->
                        <!-- Latest -->
                        <div class="col-md-4 col-xs-12 md-margin-bottom-40">
                            <div class="posts">
                                <div class="headline"><h2 class="font-robotolight"><asp:Literal runat="server" ID="litTituloSobre"/></h2></div>
                                <div class="font-robotoregular fundacao">
                                    <asp:Literal runat="server" ID="litTextoSobre"/>

                                </div>
                            </div>
                        </div><!--/col-md-3-->
                        <!-- End Latest -->
                        <!-- Link List -->
                        <div class="col-md-4 col-xs-12 md-margin-bottom-40">
                            <div class="headline"><h2 class="font-robotolight"><asp:Literal runat="server" ID="litTituloEndereco"/></h2></div>
                            <ul class="link-list font-robotoregular">
                                <li><a><asp:Literal runat="server" ID="litEndereco"/></a><i class=""></i></li>
                            </ul>
                            <ul class="telefone-rodape font-robotoregular">
                                <li><a><asp:Literal runat="server" ID="litTelefones"/></a><i class=""></i></li>
                            </ul>
                            <ul class="email-rodape font-robotoregular">
                                <li><a href="mailto:info@anybiz.com" class=""><asp:Literal runat="server" ID="litEmail"/></a><i class=""></i></li>
                            </ul>
                        </div><!--/col-md-3-->
                        <!-- End Link List -->
                        <!-- Address -->
                        <div class="col-md-4 col-xs-12 map-img md-margin-bottom-40">
                            <div class="headline"><h2 class="font-robotolight"><asp:Literal runat="server" ID="litTituloNews"/></h2></div>
                            <address class="md-margin-bottom-40 font-robotoregular">
                                <asp:Literal runat="server" ID="litTextoNews"/><br />
                            </address>

                            <div class="input-group">
                                <asp:TextBox runat="server" id="txtEmail" type="text" placeholder="Seu e-mail" class="form-control rounded"/>
                                <span class="input-group-btn">
                                    <asp:LinkButton runat="server" ID="lb_Cadastrar" CssClass="btn btn-danger rounded" OnClick="lb_Cadastrar_Click" type="button">CADASTRAR</asp:LinkButton>
                                </span>
                            </div>
                        <div class="form-group">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mensagem_erro pull-right">
                                           <asp:Literal runat="server" ID="litErro"></asp:Literal><span class="mensagem_sucesso">
                                    <asp:Literal runat="server" ID="litSucesso"></asp:Literal></span>                    
                            </div>
                        </div>
                            <div class="sociais">
                                <ul class="social-icons ">
                                    <li><a runat="server" id="linkFacebook" href="#"  target="_blank" data-original-title="Facebook" class="rounded social_facebook"></a></li>
                                    <li><a runat="server" id="linkTwitter"   href="#" target="_blank" data-original-title="Twitter" class="rounded social_twitter"></a></li>
                                    <li><a runat="server" id="linkLinkedin" href="#"  target="_blank" data-original-title="Linkedin" class="rounded social_linkedin"></a></li>
                                </ul>
                            </div>
                        </div><!--/col-md-3-->
                        <!-- End Address -->
                    </div>
                </div>
            </div><!--/footer-->

            <div class="zepol">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 zepol-div">
                            <a href="http://www.zepol.com.br" target="_blank">
                                <img src="<%# MetodosFE.BaseURL %>/assets/img/logo_zepol.png" class="img-responsive">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--=== End Footer Version 1 ===-->