﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Newsletter.ascx.cs" Inherits="ZepolControl_Newsletter" %>
<!-- Controles -->
<%--<%@ Register Src="~/ZepolControl/MenuMaster.ascx" TagName="MenuMaster" TagPrefix="uc5" %>--%>
<!-- CSS -->
<style>
    .campoNews {
        width: 380px;
        height: 30px;
        border: 0;
        padding: 6px 10px;
        font: normal lighter 18px Calibri;
        color: #CB99CB;
    }

    .MensagemAlertaCliente {
        font: normal 14px Calibri;
        color: Red;
    }
</style>
<!-- JS -->
<script>
    $(document).ready(function () {

        $("#btnEnviar").click(function () {
            if ($("#txtEmailNew").val() != "") {
                {
                    $("#lblMensagem").text("Executando...");
                    $.ajax({
                        type: 'POST'
                                    , url: "<%= MetodosFE.BaseURL %>/Webservices/Newsletter.asmx/enviaEmailNews"
                                    , contentType: 'application/json; charset=utf-8'
                                    , data: "{txtEmailNew:'" + $("#txtEmailNew").val() + "'}" //Envia a nova ordem
                                    , dataType: 'json'
                                    , success: function (data) {
                                        $("#lblMensagem").text(data.d);
                                        $("#txtEmailNew").val("");
                                    }
                                    , error: function (xmlHttpRequest, status, err) {
                                        $("#lblMensagem").text("Ocorreu algum erro no processo."); //No caso de ocorrer algum erro.
                                    }
                    });
                }
            }
            else {
                $("#lblMensagem").text("Preencha o campo.");
            }
        });

    });
</script>
<!-- Html do News -->

<div class="">
                        <div class="titulo_form_rodape">News</div>
                        <div class="titulo2_form_rodape">Receba nossas novidades</div>
                        <div class="form-horizontal" role="form">
                            <div class="form-group">
                                <div class="col-lg-12 col-md-12">
                                    <input type="text" class="form-control" id="txtEmailNew" placeholder="Digite seu e-mail">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-12 col-md-12 pull-left mensagem_erro2"><span id="lblMensagem"></span></div>
                            </div>

                            <div class="form-group">
                                <div class="col-lg-11 col-md-11 espaco_top">
                                    <a class="btn btn-info" id="btnEnviar" onclick="return false;">Cadastrar</a>
                                </div>
                            </div>
                        </div>
                    </div>

