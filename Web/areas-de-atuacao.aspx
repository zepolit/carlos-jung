﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="areas-de-atuacao.aspx.cs" Inherits="_areasDeAtuacao" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

  <!--=== CONTEÚDO ===-->
        <div class="fundo-area-de-atuacao"></div>

        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 area-de-atuacao">
                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 area-de-atuacao-menu">
                        <ul>
                           <asp:Repeater runat="server" ID="repMenu">
                              <ItemTemplate>
                            <li class="">
                                <a href="<%# String.Format("{0}/areas-de-atuacao/{1}", MetodosFE.BaseURL , ((Modelos.DadoVO)Container.DataItem).chave) %>" class="font-robotolight">
                                    <%# ((Modelos.DadoVO)Container.DataItem).nome %>
                                </a>
                            </li>
                               </ItemTemplate>
                           </asp:Repeater>
                          
                        </ul>
                    </div>
                    <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12 area-de-atuacao-conteudo">

                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 area-de-atuacao-conteudo">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 area-de-atuacao-titulo">
                            <h1 class="font-robotolight">
                                <asp:Literal runat="server" ID="litTitulo"/>
                            </h1>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 area-de-atuacao-texto">
                          <asp:Literal runat="server" ID="litTexto" />

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--=== FIM CONTEÚDO ===-->
 

</asp:Content>
