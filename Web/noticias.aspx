﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="noticias.aspx.cs" Inherits="_noticias" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


      

   <!--=== CONTEÚDO ===-->
        <div class="fundo-noticia"></div>
        <div class="container">
            <ul class="list-unstyled row portfolio-box noticia">
               <asp:Repeater runat="server" ID="repNoticias">
                  <ItemTemplate>
                <li class="col-sm-4 noticia-detalhe md-margin-bottom-50">
                    <a class="thumbnail fancybox-button zoomer" data-rel="fancybox-button" title="<%# ((Modelos.DadoVO)Container.DataItem).nome %>" href="<%# String.Format("{0}/noticia/{1}" , MetodosFE.BaseURL , ((Modelos.DadoVO)Container.DataItem).chave) %>">
                        <img class="full-width img-responsive" style="width:262px; height:263px;" src="<%# ((Modelos.DadoVO)Container.DataItem).getPrimeiraImagemLQ() %>">
                        <span class="portfolio-box-in">
                            <i class="rounded-x icon-magnifier-add"></i>
                        </span>
                    </a>
                    <div class=" texto-noticia margin-bottom-10"><h3 class="headline-brd"><%# ((Modelos.DadoVO)Container.DataItem).descricao %></h3></div>
                 
                </li>         
                </ItemTemplate>
              </asp:Repeater>      
            </ul>
            <!-- End Portfolio Box -->
        </div>
        <!--=== FIM CONTEÚDO ===-->





</asp:Content>
