﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="contato.aspx.cs" Inherits="_contato" ValidateRequest="false" %>
 
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

     <div runat="server" id="backgoundContato">
    <!--=== CONTEÚDO ===-->
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 contato">
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 contato-esquerdo" >
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 contato-endereco font-robotoregular">
                                <asp:Literal runat="server" ID="litEndereco"/>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 contato-telefone font-robotoregular">
                                <asp:Literal runat="server" ID="litTelefones"/>
                            </div>

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 contato-email font-robotoregular" style="margin-top: 6%;">
                                <a href="#"><asp:Literal runat="server" ID="litEmail"/></a>
                            </div>

                            <asp:TextBox runat="server" ID="txtnome" placeholder="Nome"  CssClass="form-control rounded"></asp:TextBox>
                            <asp:TextBox runat="server" ID="txtEmail" placeholder="Email"  CssClass="form-control rounded"></asp:TextBox>
                            <asp:TextBox runat="server" ID="txtFone" placeholder="Telefone"  CssClass="form-control rounded Telefone"></asp:TextBox>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 contato-direito">
                             <asp:TextBox runat="server" ID="txtcomentario" Rows="13" Placeholder="Comentários" CssClass="form-control rounded" TextMode="MultiLine"></asp:TextBox>
            
                            <div class=" col-lg-12 col-md-12 col-sm-12 col-xs-12 mensagem_erro pull-left font-robotoregular">
                                  <asp:Literal runat="server" ID="litErro"></asp:Literal><span class="mensagem_sucesso">
                                    <asp:Literal runat="server" ID="litSucesso"></asp:Literal></span>       
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 botao-contato text-left">
                           
                                  <asp:LinkButton ID="LinkButton1" runat="server" class="btn-u rounded font-robotoregular" OnClick="Button1_Click">Enviar contato</asp:LinkButton>
                  
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <!--=== FIM CONTEÚDO ===-->

</div>
</asp:Content>