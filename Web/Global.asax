﻿<%@ Application Language="C#" %>
<%@ Import Namespace="System.Web.Routing" %>
<%@ Import Namespace="NHibernate" %>
<%@ Import Namespace="NHibernate.Context" %>
<%@ Import Namespace="System.Net.Mail" %>
<%@ Import Namespace="Modelos" %>


<script RunAt="server">


    protected void Application_Start(object sender, EventArgs e)
    {
        RegisterRoutes(RouteTable.Routes);
    }

    public static void RegisterRoutes(RouteCollection routes)
    {


        routes.MapPageRoute("Gerenciador",
    "Controle/Cadastro/{Grupo}/{Pagina}/",
    "~/Controle/Cadastro/Telas.aspx", true);
        
        routes.MapPageRoute("",
    "Controle/Cadastro/{Grupo}/{Pagina}",
    "~/Controle/Cadastro/Telas.aspx", true);

        routes.MapPageRoute("",
            "programa/{Chave}",
            "~/programa.aspx", true);


   routes.MapPageRoute("",
            "noticia/{Chave}",
            "~/noticia.aspx", true);

  routes.MapPageRoute("",
            "areas-de-atuacao/{Chave}",
            "~/areas-de-atuacao.aspx", true);

        routes.MapPageRoute("",
            "{Pagina}/",
            "~/{Pagina}.aspx", true);


        routes.MapPageRoute("",
            "{Pagina}",
            "~/{Pagina}.aspx", true);
    }


    protected void Application_BeginRequest(object sender, EventArgs e)
    {
        //var url = Request.Url.;
        //List<String> extensoes = new List<string>();
        //extensoes.Add(".aspx");
        //extensoes.Add(".asmx");
        //extensoes.Add(".ashx");
        //extensoes.Add(".cs");
        ////extensoes.Add("");
        //string extensao = new System.IO.FileInfo(url).Extension;
        //if (extensoes.Contains(extensao))
        //{
        if (CurrentSession != null)
        {
            if (!CurrentSession.IsOpen)
                CurrentSession = NHibernateHelper.OpenSession();
        }
        else
            CurrentSession = NHibernateHelper.OpenSession();
        //}

    }

    protected void Application_EndRequest(object sender, EventArgs e)
    {

        if (CurrentSession != null)
        {
            CurrentSession.Dispose();
            System.Diagnostics.Debug.WriteLine("Session Disposed");
        }
    }


    protected void Application_AuthenticateRequest(object sender, EventArgs e)
    {

    }

    public static ISession CurrentSession
    {
        get { return (ISession)HttpContext.Current.Items["current.session"]; }
        set { HttpContext.Current.Items["current.session"] = value; }
    }

    protected void Application_Error(object sender, EventArgs e)
    {
        MailMessage msg = new MailMessage();
        HttpContext ctx = HttpContext.Current;

        HttpException lastErrorWrapper = Server.GetLastError() as HttpException;

        Exception lastError = lastErrorWrapper;
        if (lastErrorWrapper.InnerException != null)
            lastError = lastErrorWrapper.InnerException;

        string lastErrorTypeName = lastError.GetType().ToString();
        string lastErrorMessage = lastError.Message;
        string lastErrorStackTrace = lastError.StackTrace;

        if (lastErrorTypeName != "System.Web.HttpException")
        {
            DadoVO dado = MetodosFE.getTela("Configurações de SMTP");

            msg.To.Add(new MailAddress(Configuracoes.getSetting("EmailErro")));
            msg.From = new MailAddress(dado.referencia, "Controle de Erro");
            msg.Subject = "EXCEPTION: " + Configuracoes.getSetting("NomeSite") + " - " + lastErrorTypeName;
            msg.Priority = MailPriority.High;
            msg.IsBodyHtml = true;
            msg.Body = string.Format(@"
  <h1>ONE DAMN BIG Exception Appeared!!</h1> 
  <table cellpadding=""5"" cellspacing=""0"" border=""1""> 
  <tr> 
  <td text-align: right;font-weight: bold"">URL:</td> 
  <td>{0}</td> 
  </tr> 
  <tr> 
  <td text-align: right;font-weight: bold"">User:</td> 
  <td>{1}</td> 
  </tr> 
  <tr> 
  <td text-align: right;font-weight: bold"">Exception Type:</td> 
  <td>{2}</td> 
  </tr> 
  <tr> 
  <td text-align: right;font-weight: bold"">Message:</td> 
  <td>{3}</td> 
  </tr> 
  <tr> 
  <td text-align: right;font-weight: bold"">Stack Trace:</td> 
  <td>{4}</td> 
  </tr>  
  </table>",
                        Request.Url,
                        Request.ServerVariables["LOGON_USER"],
                        lastErrorTypeName,
                        lastErrorMessage,
                        lastErrorStackTrace.Replace(Environment.NewLine, "<br />"));


            // Attach the Yellow Screen of Death for this error    
            string YSODmarkup = lastErrorWrapper.GetHtmlErrorMessage();
            if (!string.IsNullOrEmpty(YSODmarkup))
            {
                Attachment YSOD = Attachment.CreateAttachmentFromString(YSODmarkup, "YSOD.htm");
                msg.Attachments.Add(YSOD);
            }

            //CONFIGURE SMTP OBJECT
            System.Net.Mail.SmtpClient objSmtp = new System.Net.Mail.SmtpClient();

            dado = MetodosFE.getTela("Configuração de E-mail");
            if (dado != null)
            {
                objSmtp.Host = dado.nome;
                objSmtp.Port = Convert.ToInt32(dado.valor);
                objSmtp.Credentials = new System.Net.NetworkCredential(dado.referencia, dado.ordem);
            }
            else
                throw new Exception("Problemas ocorreram na configuração de E-mail.");

            //SEND EMAIL
            objSmtp.Send(msg);

            //REDIRECT USER TO ERROR PAGE
            //Server.Transfer("~/ErrorPage.aspx");
        }
    }

    protected void Session_End(object sender, EventArgs e)
    {

    }

    protected void Application_End(object sender, EventArgs e)
    {

    }
</script>
