﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using NHibernate.Linq;
using System.Linq.Dynamic;
using System.Collections.Generic;
using Modelos;

public partial class Controle_Cadastro_Estado : System.Web.UI.Page
{
    public string nome { get; set; }
    public string nome2 { get; set; }


    private Repository<GrupoDePaginasVO> repoGrupoPaginas
    {
        get
        {
            return new Repository<GrupoDePaginasVO>(NHibernateHelper.CurrentSession);
        }
    }
    private Repository<PaginaDeControleVO> repoPaginasControle
    {
        get
        {
            return new Repository<PaginaDeControleVO>(NHibernateHelper.CurrentSession);
        }
    }

    //private Repository<EstadoVO> repoEstado
    //{
    //    get
    //    {
    //        return new Repository<EstadoVO>(NHibernateHelper.CurrentSession);
    //    }
    //}
    //private Repository<CidadeVO> repoCidade
    //{
    //    get
    //    {
    //        return new Repository<CidadeVO>(NHibernateHelper.CurrentSession);
    //    }
    //}

    protected void Page_Load(object sender, EventArgs e)
    {
        nome = "Tela das Demais Telas";
        nome2 = "Tela de Demais Telas";
        this.MaintainScrollPositionOnPostBack = true;
        this.Title = nome;
        litTitulo.Text = nome;
        if (!Page.IsPostBack)
        {
            //carregarGrupoPaginas();
            Carregar();
        }
    }

    protected virtual void Page_LoadComplete(object sender, EventArgs e)
    {
        string mensagem = MetodosFE.confereMensagem();
        litErro.Text = mensagem != null ? mensagem : "";
    }

    protected void Carregar()
    {
        try
        {
            PaginaDeControleVO pagina;


            pagina = repoPaginasControle.FindBy(x => x.nome == "Usuários" && x.fixa);

            if (pagina != null)
            {
                chkUsuarios.Checked = true;
                //ddlUsuariosGrupo.SelectedValue = pagina.grupoDePaginas.id.ToString();
            }

            pagina = repoPaginasControle.FindBy(x => x.nome == "Controle de Acesso" && x.fixa);

            if (pagina != null)
            {
                chkControleAcesso.Checked = true;
                //ddlControleAcessoGrupo.SelectedValue = pagina.grupoDePaginas.id.ToString();
            }

            pagina = repoPaginasControle.FindBy(x => x.nome == "Estados" && x.fixa);

            if (pagina != null)
            {
                chkEstados.Checked = true;
                //ddlEstadosGrupo.SelectedValue = pagina.grupoDePaginas.id.ToString();
            }

            pagina = repoPaginasControle.FindBy(x => x.nome == "Controle de Sitemap" && x.fixa);

            if (pagina != null)
            {
                chkControleSiteMap.Checked = true;
                //ddlEstadosGrupo.SelectedValue = pagina.grupoDePaginas.id.ToString();
            }

            pagina = repoPaginasControle.FindBy(x => x.nome == "Cidades" && x.fixa);

            if (pagina != null)
            {
                chkCidades.Checked = true;
                //ddlCidadesGrupo.SelectedValue = pagina.grupoDePaginas.id.ToString();
            }

            pagina = repoPaginasControle.FindBy(x => x.nome == "Bairros" && x.fixa);

            if (pagina != null)
            {
                chkBairros.Checked = true;
                //ddlBairrosGrupo.SelectedValue = pagina.grupoDePaginas.id.ToString();
            }

            btnAlterar.Visible = true;
        }
        catch (Exception er)
        {
            MetodosFE.mostraMensagem(er.Message);
        }
    }

    protected void btnAlterar_Click(object sender, EventArgs e)
    {
        try
        {
            PaginaDeControleVO pagina = null;

            pagina = repoPaginasControle.FindBy(x => x.nome == "Usuários" && x.fixa);
            if (chkUsuarios.Checked)
            {
                if (pagina == null)
                {
                    pagina = new PaginaDeControleVO() { nome = "Usuários", fixa = true, pagina = MetodosFE.BaseURL + "/Controle/Cadastro/Usuarios.aspx" };
                    repoPaginasControle.Add(pagina);
                }
                //pagina.grupoDePaginas = new GrupoDePaginasVO() { id = Convert.ToInt32(ddlUsuariosGrupo.SelectedValue) };
                repoPaginasControle.Update(pagina);
            }
            else
                if (pagina != null)
                    repoPaginasControle.Delete(pagina);

            pagina = repoPaginasControle.FindBy(x => x.nome == "Controle de Acesso" && x.fixa);
            if (chkControleAcesso.Checked)
            {
                if (pagina == null)
                {
                    pagina = new PaginaDeControleVO() { nome = "Controle de Acesso", fixa = true, pagina = MetodosFE.BaseURL + "/Controle/Cadastro/ControleAcesso.aspx" };
                    repoPaginasControle.Add(pagina);
                }
                //pagina.grupoDePaginas = new GrupoDePaginasVO() { id = Convert.ToInt32(ddlControleAcessoGrupo.SelectedValue) };
                repoPaginasControle.Update(pagina);
            }
            else
                if (pagina != null)
                    repoPaginasControle.Delete(pagina);

            pagina = repoPaginasControle.FindBy(x => x.nome == "Estados" && x.fixa);
            if (chkEstados.Checked)
            {
                if (pagina == null)
                {
                    pagina = new PaginaDeControleVO() { nome = "Estados", fixa = true, pagina = MetodosFE.BaseURL + "/Controle/Cadastro/Estados.aspx" };
                    repoPaginasControle.Add(pagina);
                }
                //pagina.grupoDePaginas = new GrupoDePaginasVO() { id = Convert.ToInt32(ddlEstadosGrupo.SelectedValue) };
                repoPaginasControle.Update(pagina);
            }
            else
                if (pagina != null)
                    repoPaginasControle.Delete(pagina);

            pagina = repoPaginasControle.FindBy(x => x.nome == "Cidades" && x.fixa);
            if (chkCidades.Checked)
            {
                if (pagina == null)
                {
                    pagina = new PaginaDeControleVO() { nome = "Cidades", fixa = true, pagina = MetodosFE.BaseURL + "/Controle/Cadastro/Cidades.aspx" };
                    repoPaginasControle.Add(pagina);
                }
                //pagina.grupoDePaginas = new GrupoDePaginasVO() { id = Convert.ToInt32(ddlCidadesGrupo.SelectedValue) };
                repoPaginasControle.Update(pagina);
            }
            else
                if (pagina != null)
                    repoPaginasControle.Delete(pagina);

            

                pagina = repoPaginasControle.FindBy(x => x.nome == "Controle de Sitemap" && x.fixa);
                if (chkControleSiteMap.Checked)
            {
                if (pagina == null)
                {
                    pagina = new PaginaDeControleVO() { nome = "Controle de Sitemap", fixa = true, pagina = MetodosFE.BaseURL + "/Controle/Cadastro/ControleSitemap.aspx" };
                    repoPaginasControle.Add(pagina);
                }
                //pagina.grupoDePaginas = new GrupoDePaginasVO() { id = Convert.ToInt32(ddlBairrosGrupo.SelectedValue) };
                repoPaginasControle.Update(pagina);
            }
            else
                if (pagina != null)
                    repoPaginasControle.Delete(pagina);


            pagina = repoPaginasControle.FindBy(x => x.nome == "Bairros" && x.fixa);
            if (chkBairros.Checked)
            {
                if (pagina == null)
                {
                    pagina = new PaginaDeControleVO() { nome = "Bairros", fixa = true, pagina = MetodosFE.BaseURL + "/Controle/Cadastro/Bairros.aspx" };
                    repoPaginasControle.Add(pagina);
                }
                //pagina.grupoDePaginas = new GrupoDePaginasVO() { id = Convert.ToInt32(ddlBairrosGrupo.SelectedValue) };
                repoPaginasControle.Update(pagina);
            }
            else
                if (pagina != null)
                    repoPaginasControle.Delete(pagina);

            MetodosFE.mostraMensagem(nome2 + " alterado com sucesso.", "sucesso");
            this.Limpar();

        }
        catch (Exception er)
        {
            MetodosFE.mostraMensagem(er.Message);
        }
    }

    protected void btnCancelar_Click(object sender, EventArgs e)
    {
        try
        {
            this.Limpar();
        }
        catch (Exception er)
        {
            MetodosFE.mostraMensagem(er.Message);
        }
    }

    protected void Limpar()
    {
        var nameValues = HttpUtility.ParseQueryString(Request.QueryString.ToString());
        MetodosFE.recuperaMensagem();
        nameValues.Remove("Codigo");
        string url = Request.Url.AbsolutePath;
        //nameValues.Remove("Codigo");
        string updatedQueryString = "";
        if (nameValues.Count > 0)
            updatedQueryString = "?" + nameValues.ToString();

        string urlFinal = url + updatedQueryString;
        Response.Redirect(urlFinal, false);
    }

    #region Guardamos o Código no ViewState
    private int Codigo
    {
        get
        {
            if (ViewState["Codigo"] == null) ViewState["Codigo"] = 0;
            return (Int32)ViewState["Codigo"];
        }
        set { ViewState["Codigo"] = value; }
    }

    private int Deleta
    {
        get
        {
            if (ViewState["Deleta"] == null) ViewState["Deleta"] = 0;
            return (Int32)ViewState["Deleta"];
        }
        set { ViewState["Deleta"] = value; }
    }
    private int Pagina
    {
        get
        {
            if (Session["Pagina"] == null) Session["Pagina"] = 0;
            return (Int32)Session["Pagina"];
        }
        set { Session["Pagina"] = value; }
    }

    #endregion

}
