﻿<%@ Page Language="C#" MasterPageFile="~/Controle/GerenciadorNovo.master" AutoEventWireup="true" CodeFile="PaginaDemais.aspx.cs" Inherits="Controle_Cadastro_Estado" Title="Acabamentos" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphConteudo" runat="Server">
    <script type="text/javascript">
        $(document).ready(
				function () {
				    $('#divDados').show();
				    $('#tituloCadastro').css('color', '#FFF');
				});


    </script>
    <asp:HiddenField ID="hfSecao" ClientIDMode="Static" runat="server" />
    <asp:Literal runat="server" ID="litErro"></asp:Literal>
    <h1 class="TituloPagina">
        <asp:Literal runat="server" ID="litTitulo"></asp:Literal>
    </h1>

    <h1 id="tituloCadastro" class="TituloSecao">Cadastro
    </h1>
    <div id="divDados" runat="server" clientidmode="Static">
        <ul>
            <li>
                <table>
                    <tbody>
                        <tr>
                            <th>Tela</th>
                            <th>Ativo?</th>
                        </tr>
                        <tr>
                            <td>
                                <span class="nomeCampo">Controle do SiteMap</span>
                            </td>
                            <td>
                                <asp:CheckBox ID="chkControleSiteMap" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="nomeCampo">Usuários</span>
                            </td>
                            <td>
                                <asp:CheckBox ID="chkUsuarios" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="nomeCampo">Controle de Acesso</span>
                            </td>
                            <td>
                                <asp:CheckBox ID="chkControleAcesso" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="nomeCampo">Estados</span>
                            </td>
                            <td>
                                <asp:CheckBox ID="chkEstados" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="nomeCampo">Cidades</span>
                            </td>
                            <td>
                                <asp:CheckBox ID="chkCidades" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="nomeCampo">Bairros</span>
                            </td>
                            <td>
                                <asp:CheckBox ID="chkBairros" runat="server" />
                            </td>
                        </tr>
                        
                    </tbody>
                </table>
            </li>
        </ul>
        <div>
            <asp:Button ID="btnAlterar" runat="server" Text="Alterar"
                OnClick="btnAlterar_Click" CssClass="EstiloBotao" />
            <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" CssClass="EstiloBotao"
                OnClick="btnCancelar_Click" />

        </div>
    </div>
</asp:Content>

