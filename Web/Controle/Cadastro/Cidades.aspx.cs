﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using NHibernate.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using NHibernate.Linq;
using System.Linq.Dynamic;
using System.Collections.Generic;
using Modelos;

public partial class Controle_Cadastro_Cidade : System.Web.UI.Page
{

    private Repository<CidadeVO> repoCidade 
    {
        get 
        {
            return new Repository<CidadeVO>(NHibernateHelper.CurrentSession);
        }
    }
    private Repository<EstadoVO> repoEstado
    {
        get
        {
            return new Repository<EstadoVO>(NHibernateHelper.CurrentSession);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (!String.IsNullOrEmpty(Request.QueryString["Codigo"]))
            {
                Codigo = Convert.ToInt32(Request.QueryString["Codigo"].ToString());
                Carregar();
                Pesquisar("nome");
            }
            else
            {
                Pesquisar("nome");
                CarregarDropEstado();
                btnAlterar.Visible = false;
                btnPesquisar.Visible = true;
                btnSalvar.Visible = true;
            }
        }
        else
        {
            Pesquisar("nome");
            btnAlterar.Visible = false;
            btnPesquisar.Visible = true;
            btnSalvar.Visible = true;
        }
    }

    protected void Pesquisar(string ordenacao)
    {
        try
        {
            CidadeVO cidade = new CidadeVO();


            IList<CidadeVO> colecaoCidade = repoCidade.All().OrderBy(ordenacao).ToList();


                gvCidade.DataSourceID = String.Empty;
                gvCidade.DataSource = colecaoCidade;
                gvCidade.DataBind();

        }
        catch (Exception er)
        {
            MetodosFE.mostraMensagem(er.Message);
        }
    }

    protected void Carregar()
    {
        try
        {
            CidadeVO cidade = repoCidade.FindBy(Codigo);

            if (cidade!=null)
            {

                txtCidade.Text = cidade.nome;
                DropEstado.Text = cidade.estado.id.ToString();
                txtId.Text = cidade.id.ToString(); 


                btnSalvar.Visible = false;
                btnPesquisar.Visible = false;
                btnAlterar.Visible = true;
            }
        }
        catch (Exception er)
        {
            MetodosFE.mostraMensagem(er.Message);
        }
    }

    protected void CarregarDropEstado()
    {
        try
        {
            //Prenche o drop estado.

            IList<EstadoVO> colecaoEstado = repoEstado.All().OrderBy("id").ToList();

            if (colecaoEstado.Count > 0)
            {
                DropEstado.DataSourceID = String.Empty;
                DropEstado.DataSource = colecaoEstado;
                DropEstado.DataTextField = "nome";
                DropEstado.DataValueField = "id";
                DropEstado.DataBind();
                DropEstado.Items.Insert(0, new ListItem("--Selecione--", "-1"));
            }
        }
        catch (Exception er)
        {
            MetodosFE.mostraMensagem(er.Message);
        }
    }

    protected void gvCidade_Sorting(object sender, GridViewSortEventArgs e)
    {
        string ordenacao = e.SortExpression;
        Pesquisar(ordenacao);
    }

    protected void btnPesquisar_Click(object sender, EventArgs e)
    {
        Pesquisar("nome");
    }

    protected void btnSalvar_Click(object sender, EventArgs e)
    {
        try
        {
            CidadeVO cidade = new CidadeVO();

            if ((txtCidade.Text != ""))
            {
                cidade.nome = txtCidade.Text.Trim();
                cidade.estado = repoEstado.FindBy(Convert.ToInt32(DropEstado.SelectedValue));

                repoCidade.Add(cidade);
                MetodosFE.mostraMensagem("Cidade "+cidade.nome+" cadastrada com sucesso.", "sucesso");
                this.Limpar();

            }
            else
            {
                MetodosFE.mostraMensagem("Campos Obrigatórios.") ;
            }

        }
        catch (Exception er)
        {
            MetodosFE.mostraMensagem(er.Message);
        }


    }

    protected void btnAlterar_Click(object sender, EventArgs e)
    {
        try
        {
            CidadeVO cidade = new CidadeVO();

            if ((txtCidade.Text != ""))
            {
                cidade.id = Convert.ToInt32(txtId.Text);
                cidade.nome = txtCidade.Text.Trim();
                cidade.estado = repoEstado.FindBy(Convert.ToInt32(DropEstado.SelectedValue));
                repoCidade.Update(cidade);
                MetodosFE.mostraMensagem("Dados alterados com sucesso.", "sucesso");
                Limpar();
            }
            else
            {
                MetodosFE.mostraMensagem(" Campos Obrigatórios."); 
            }
        }
        catch (Exception er)
        {
            MetodosFE.mostraMensagem(er.Message);
        }
    }

    protected void btnCancelar_Click(object sender, EventArgs e)
    {
        try
        {
            CarregarDropEstado();
            Pesquisar("nome");
            Limpar();
        }
        catch { throw; }
    }

    protected void Limpar()
    {

        var nameValues = HttpUtility.ParseQueryString(Request.QueryString.ToString());
        MetodosFE.recuperaMensagem();
        nameValues.Remove("Codigo");
        string url = Request.Url.AbsolutePath;
        //nameValues.Remove("Codigo");
        string updatedQueryString = "";
        if (nameValues.Count > 0)
            updatedQueryString = "?" + nameValues.ToString();

        string urlFinal = url + updatedQueryString;
        Response.Redirect(urlFinal, false);

    }

    #region Guardamos o Código no ViewState
    private int Codigo
    {
        get
        {
            if (ViewState["Codigo"] == null) ViewState["Codigo"] = 0;
            return (Int32)ViewState["Codigo"];
        }
        set { ViewState["Codigo"] = value; }
    }

    private int Deleta
    {
        get
        {
            if (ViewState["Deleta"] == null) ViewState["Deleta"] = 0;
            return (Int32)ViewState["Deleta"];
        }
        set { ViewState["Deleta"] = value; }
    }

    #endregion

    protected void gvCidade_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {

        }
    }

    protected void gvCidade_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvCidade.PageIndex = e.NewPageIndex;

        Pesquisar("nome");
    }

    protected void gvCidade_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            CidadeVO cor = repoCidade.FindBy(Convert.ToInt32(gvCidade.DataKeys[e.RowIndex].Value));
            repoCidade.Delete(cor);
            MetodosFE.mostraMensagem("Cidade " + cor.nome + " alterado com sucesso.", "sucesso");
            Limpar();
        }
        catch (Exception er)
        {
            MetodosFE.mostraMensagem(er.Message);
        }
    }
    protected void gvCidade_RowEditing(object sender, GridViewEditEventArgs e)
    {
        Response.Redirect(this.AppRelativeVirtualPath + "?Codigo=" + Convert.ToInt32(gvCidade.DataKeys[e.NewEditIndex].Value), false);
    }
}
