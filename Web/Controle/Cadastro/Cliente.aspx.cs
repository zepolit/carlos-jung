﻿using System;
using System.Configuration;
using System.Data;
using System.Web.Security;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls.WebParts;
using System.Collections.ObjectModel;
using System.Globalization;

using System.Linq;
using NHibernate.Linq;
using System.Linq.Dynamic;
using System.Collections.Generic;
using Modelos;

public partial class Controle_Cadastro_Categorias : System.Web.UI.Page
{
    private Repository<ClienteVO> repoCliente
    {
        get
        {
            return new Repository<ClienteVO>(NHibernateHelper.CurrentSession);
        }
    }
    private Repository<EnderecoVO> repoEndereco
    {
        get
        {
            return new Repository<EnderecoVO>(NHibernateHelper.CurrentSession);
        }
    }
    private Repository<ContatoVO> repoContato
    {
        get
        {
            return new Repository<ContatoVO>(NHibernateHelper.CurrentSession);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            
            txtID.Enabled = false;
            if (!String.IsNullOrEmpty(Request.QueryString["Codigo"]))
            {
                Codigo = Convert.ToInt32(Request.QueryString["Codigo"].ToString());
                Carregar();
                Pesquisar("id");
            }
            else
            {
                try
                {
                    Pesquisar("id");
                    //CarregarDropSegmentoFilho();  
                    btnAlterar.Visible = false;
                    btnPesquisar.Visible = true;
                    btnSalvar.Visible = true;
                }
                catch (Exception er)
                {
                    MetodosFE.mostraMensagem(er.Message);
                }
            }
        }
    }
    protected virtual void Page_LoadComplete(object sender, EventArgs e)
    {
        string mensagem = MetodosFE.confereMensagem();
        litErro.Text = mensagem != null ? mensagem : "";
    }



    protected void Pesquisar(string ordenacao)
    {
        try
        {
            int id = 0;
            if (!String.IsNullOrEmpty(txtBuscaID.Text)) 
            {
                id = Convert.ToInt32(txtBuscaID.Text);
            }
            string nome = null;
            if (!String.IsNullOrEmpty(txtBuscaNome.Text))
            {
                nome = txtBuscaNome.Text;
            }
            string CPFCNPJ = null;
            if (!String.IsNullOrEmpty(txtBuscaCPFCNPJ.Text)) 
            {
                CPFCNPJ = txtBuscaCPFCNPJ.Text;
            }


            IList<ClienteVO> colecaoSegmento = repoCliente.FilterBy(x=> 
                (id > 0 && x.id == id || id == 0) 
                && ( !String.IsNullOrEmpty(nome) ? x.nome.Contains(nome) : true)
                && ( !String.IsNullOrEmpty(CPFCNPJ) ? x.CPFCNPJ.Contains(CPFCNPJ):true))
                .OrderBy(ordenacao).ToList();

                gvSegmento.DataSourceID = String.Empty;
                gvSegmento.DataSource = colecaoSegmento;
                gvSegmento.DataBind();
            
        }
        catch (Exception er)
        {
            MetodosFE.mostraMensagem(er.Message);
        }
    }

    protected void Carregar()
    {

        int idContato = 0;
        int idEstado = 0;
        try
        {


            ClienteVO cliente = null;

            cliente = repoCliente.FindBy(Codigo);
            

            if (cliente!=null)
            {
                txtID.Text = cliente.id.ToString();
                txtNome.Text = cliente.nome;
                txtCPF.Text = cliente.CPFCNPJ.ToString();

                Contato.carregarContato(cliente.contato.id);
                Endereco.carregarEndereco(cliente.endereco.id);
                txtObservacoes.Text = cliente.observacao;
                ddlStatus.SelectedValue = cliente.status;
                //ControleLoja.setIDLoja(cliente.idLoja);
                //txtDataPagamento.Text = cliente.dtpagamento.ToString();
                

                btnSalvar.Visible = false;
                btnPesquisar.Visible = false;
                btnAlterar.Visible = true;
                txtCPF.Enabled = false;
            }
        }
        catch (Exception er)
        {
            MetodosFE.mostraMensagem(er.Message);
        }
    }

    protected void gvSegmento_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            repoCliente.Delete(repoCliente.FindBy(Convert.ToInt32(gvSegmento.DataKeys[e.RowIndex].Value)));
        }
        catch (Exception er)
        {
            MetodosFE.mostraMensagem(er.Message);
        }


        gvSegmento.DataBind();
        Pesquisar("id");
    }

    protected void gvSegmento_Sorting(object sender, GridViewSortEventArgs e)
    {
        string ordenacao = e.SortExpression;
        Pesquisar(ordenacao);
    }
    protected void gvDados_RowEditing(object sender, GridViewEditEventArgs e)
    {

        Codigo = Convert.ToInt32(gvSegmento.DataKeys[e.NewEditIndex].Value);

        var nameValues = HttpUtility.ParseQueryString(Request.QueryString.ToString());
        nameValues.Set("Codigo", Codigo.ToString());
        string url = Request.Url.AbsolutePath;
        //nameValues.Remove("Codigo");
        string updatedQueryString = "?" + nameValues.ToString();
        string urlFinal = url + updatedQueryString;
        e.Cancel = true;
        Response.Redirect(urlFinal,false);

        //Carregar();

    }

    protected void btnPesquisar_Click(object sender, EventArgs e)
    {
        Pesquisar("id");
    }

    protected void btnSalvar_Click(object sender, EventArgs e)
    {

        int idContato = 0;
        int idEndereco = 0;
        try
        {
            ClienteVO cliente = new ClienteVO();


                cliente.nome=txtNome.Text;
                cliente.observacao = HttpUtility.HtmlDecode(txtObservacoes.Text);
                cliente.status = ddlStatus.SelectedValue;
                cliente.CPFCNPJ = txtCPF.Text.Replace('_', ' ').Trim();
                //cliente.idLoja = ControleLoja.getIDLoja();
                idContato = Contato.salvarContato();
                idEndereco = Endereco.salvarEndereco();

                cliente.contato = repoContato.FindBy(idContato);
                cliente.endereco = repoEndereco.FindBy(idEndereco);

                //cliente.dtpagamento = txtDataPagamento.Text;

                repoCliente.Add(cliente);
                Limpar();

        }
        catch (Exception er)
        {

            if(idContato>0)
                repoContato.Delete(repoContato.FindBy(idContato));
            if (idEndereco > 0)
                repoEndereco.Delete(repoEndereco.FindBy(idEndereco));
            MetodosFE.mostraMensagem(er.Message);

            
        }
    }

    protected void btnAlterar_Click(object sender, EventArgs e)
    {
        try
        {
            ClienteVO cliente = repoCliente.FindBy(Convert.ToInt32(txtID.Text));

           
            cliente.nome = txtNome.Text;
            cliente.CPFCNPJ = txtCPF.Text.Replace('_', ' ').Trim() ;
            cliente.observacao = HttpUtility.HtmlDecode(txtObservacoes.Text);
            //cliente.idLoja = ControleLoja.getIDLoja();
            cliente.status = ddlStatus.SelectedValue;
            Contato.atualizarContato();
            Endereco.atualizarEndereco();

            
            //cliente.perccomissao = Convert.ToInt32(txtPercComissao.Text);
            
            //cliente.dtpagamento = txtDataPagamento.Text;

            repoCliente.Update(cliente);

                this.Limpar();
        }
        catch (Exception er)
        {
            MetodosFE.mostraMensagem(er.Message);
        }
    }

    protected void btnCancelar_Click(object sender, EventArgs e)
    {
        try
        {
            this.Limpar();
        }
        catch (Exception er)
        {
            MetodosFE.mostraMensagem(er.Message);
        }
    }

    protected void Limpar()
    {

        var nameValues = HttpUtility.ParseQueryString(Request.QueryString.ToString());
        MetodosFE.recuperaMensagem();
        nameValues.Remove("Codigo");
        string url = Request.Url.AbsolutePath;
        //nameValues.Remove("Codigo");
        string updatedQueryString = "";
        if (nameValues.Count > 0)
            updatedQueryString = "?" + nameValues.ToString();

        string urlFinal = url + updatedQueryString;
        Response.Redirect(urlFinal, false);

    }


    #region Guardamos o Código no ViewState
    private int Codigo
    {
        get
        {
            if (ViewState["Codigo"] == null) ViewState["Codigo"] = 0;
            return (Int32)ViewState["Codigo"];
        }
        set { ViewState["Codigo"] = value; }
    }
    #endregion

    protected void gvSegmento_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {

        }
    }

    protected void gvSegmento_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvSegmento.PageIndex = e.NewPageIndex;
            Pesquisar("id");
        }
        catch (Exception er)
        {
            MetodosFE.mostraMensagem(er.Message);
        }
    }
}
