﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using NHibernate.Linq;
using System.Linq.Dynamic;
using System.Collections.Generic;
using Modelos;
using System.IO;
using System.Xml;

public partial class Controle_Cadastro_Estado : System.Web.UI.Page
{
    public string nome { get; set; }
    public string nome2 { get; set; }



    protected void Page_Load(object sender, EventArgs e)
    {
        nome = "Controle de SiteMap";
        nome2 = "Controle de SiteMap";
        this.MaintainScrollPositionOnPostBack = true;
        this.Title = nome;
        litTitulo.Text = nome;

        if (!IsPostBack)
        {
            string caminhoBase = HttpContext.Current.Server.MapPath("~/sitemap.xml");
            XmlDocument xml = new XmlDocument();
            FileInfo teste = new FileInfo(caminhoBase);
            if (teste.Exists)
            {
                linkXML.HRef = MetodosFE.BaseURL + "/sitemap.xml";
                linkXML.Target = "_blank";
                linkXML.InnerText = "Clique para ver o XML atual.";
            }
        }
    }


    protected void btnCarregar_Click(object sender, EventArgs e)
    {
        try
        {

            string caminhoBase = HttpContext.Current.Server.MapPath("~/sitemap.xml");

            //File 

            if (fulSiteMap.HasFile)
            {
                bool bValido = false;

                string fileExtension = System.IO.Path.GetExtension(fulSiteMap.FileName).ToLower();
                foreach (string ext in new string[] { ".xml" })
                {
                    if (fileExtension == ext)
                        bValido = true;
                }
                if (!bValido)
                    throw new Exception("Extensão inválida de arquivo.");
            }

            fulSiteMap.SaveAs(caminhoBase);

            linkXML.HRef = MetodosFE.BaseURL + "/sitemap.xml";
            linkXML.Target = "_blank";
            linkXML.InnerText = "Clique para ver o XML atual.";

            MetodosFE.mostraMensagem("Sitemap carregado.", "sucesso");



        }
        catch (Exception ex)
        {
            litErro.Text = (ex.Message);
        }
    }
}
