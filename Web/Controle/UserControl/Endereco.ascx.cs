﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

using System.Linq;
using NHibernate.Linq;
using System.Linq.Dynamic;
using Modelos;

public partial class ZepolControl_DadosTexto : System.Web.UI.UserControl
{
    private Repository<EnderecoVO> repoEndereco
    {
        get
        {
            return new Repository<EnderecoVO>(NHibernateHelper.CurrentSession);
        }
    }
    private Repository<CidadeVO> repoCidade
    {
        get
        {
            return new Repository<CidadeVO>(NHibernateHelper.CurrentSession);
        }
    }
    private Repository<EstadoVO> repoEstado
    {
        get
        {
            return new Repository<EstadoVO>(NHibernateHelper.CurrentSession);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }
    #region Guardamos o Código no ViewState
    public int Codigo
    {
        get
        {
            if (ViewState["Codigo"] == null) ViewState["Codigo"] = 0;
            return (Int32)ViewState["Codigo"];
        }
        set { ViewState["Codigo"] = value; }
    }
    #endregion
    public void carregarEndereco(int idEndereco)
    {
        if (idEndereco > 0)
        {
            Codigo = idEndereco;
            EnderecoVO endereco = repoEndereco.FindBy(Codigo);
            txtEndereco.Text = endereco.endereco;
            txtCEP.Text = endereco.CEP;
            txtBairro.Text = endereco.bairro;
            txtNumero.Text = endereco.numero.ToString();
            txtComplemento.Text = endereco.complemento;
            CidadeEstado.carregarEstados(endereco.estado.id);
            CidadeEstado.carregarCidades(endereco.cidade.id);
        }
    }

    public int salvarEndereco()
    {
        validaCampos();
        EnderecoVO endereco = new EnderecoVO();
        endereco.endereco = txtEndereco.Text;
        endereco.bairro = txtBairro.Text;
        endereco.numero = Convert.ToInt32(txtNumero.Text);
        endereco.CEP = txtCEP.Text;
        endereco.complemento = txtComplemento.Text;
        endereco.estado = repoEstado.FindBy(CidadeEstado.getEstadoID());
        endereco.cidade = repoCidade.FindBy(CidadeEstado.getCidadeID());
        repoEndereco.Add(endereco);
        return Codigo;

    }
    public void atualizarEndereco()
    {
        validaCampos();
        EnderecoVO endereco = repoEndereco.FindBy(Codigo);
        endereco.endereco = txtEndereco.Text;
        endereco.bairro = txtBairro.Text;
        endereco.numero = Convert.ToInt32(txtNumero.Text);
        endereco.CEP = txtCEP.Text;
        endereco.complemento = txtComplemento.Text;
        endereco.estado = repoEstado.FindBy(CidadeEstado.getEstadoID());
        endereco.cidade = repoCidade.FindBy(CidadeEstado.getCidadeID());
        repoEndereco.Update(endereco);
    }

    public void validaCampos()
    {
        string mensagemErro = "";
        if (String.IsNullOrEmpty(txtEndereco.Text))
            mensagemErro += "Endereço <br/>";
        if (String.IsNullOrEmpty(txtNumero.Text))
            mensagemErro += "Número <br/>";
        if (String.IsNullOrEmpty(txtBairro.Text))
            mensagemErro += "Bairro <br/>";
        if (String.IsNullOrEmpty(txtCEP.Text))
            mensagemErro += "CEP <br/>";
        if (CidadeEstado.getEstadoID() <= 0)
            mensagemErro += "Estado <br/>";
        if (CidadeEstado.getCidadeID() <= 0)
            mensagemErro += "Cidade <br/>";
        if (!String.IsNullOrEmpty(mensagemErro)) 
        {
            throw new Exception("Verifique os seguintes campos: <br/>" + mensagemErro);
        }
    }

    public void excluirEndereco() 
    {
        if (Codigo != 0)
            repoEndereco.Delete(repoEndereco.FindBy(Codigo));
    }

}
