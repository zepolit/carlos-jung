﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Endereco.ascx.cs" Inherits="ZepolControl_DadosTexto" %>
<!-- Controles -->
<%@ Register Src="~/Controle/UserControl/CidadeEstado.ascx" TagName="CidadeEstado"
    TagPrefix="uc1" %>
<!-- CSS -->
<!-- JS -->
<!-- Html do Cabecalho -->
<li>
    <asp:Label ID="Label8" runat="server" Text="Endereço:" CssClass="nomeCampo"></asp:Label>
    <asp:TextBox runat="server" ID="txtEndereco" Style="width: 600px;"></asp:TextBox>
</li>
<li>
        <asp:Label ID="Label1" runat="server" Text="Número:" CssClass="nomeCampo"></asp:Label>
    <asp:TextBox runat="server" ID="txtNumero" CssClass="Numero" ></asp:TextBox>
            <asp:Label ID="Label2" runat="server" Text="Complemento:" CssClass="nomeCampo"></asp:Label>
    <asp:TextBox runat="server" ID="txtComplemento"  ></asp:TextBox>
</li>
<li>
    <asp:Label ID="Label9" runat="server" Text="Bairro:" CssClass="nomeCampo"></asp:Label>
    <asp:TextBox runat="server" ID="txtBairro" Style="width: 400px;"></asp:TextBox>
    <asp:Label ID="Label10" runat="server" Text="CEP:" CssClass="nomeCampo" Style="width: 70px;"></asp:Label>
    <asp:TextBox runat="server" ID="txtCEP" CssClass="CEP"></asp:TextBox>
</li>
<uc1:CidadeEstado ID="CidadeEstado" runat="server" />
