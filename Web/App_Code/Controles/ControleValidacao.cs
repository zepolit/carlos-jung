﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using Modelos;

/// <summary>
/// Summary description for ControleValidacao
/// </summary>
public class ControleValidacao
{
    private static Repository<UsuarioVO> repUsuario
    {
        get
        {
            return new Repository<UsuarioVO>(NHibernateHelper.CurrentSession);
        }
    }
    private static Repository<ClienteVO> repCliente
    {
        get
        {
            return new Repository<ClienteVO>(NHibernateHelper.CurrentSession);
        }
    }

    public static String validaTelefone(String telefone)
    {
        //Formato (99)9999-9999X
        //        01234567890123    
        if (telefone.Length != 13 && telefone.Length != 14)
            return "";

        for (int i = 0; i < telefone.Length; i++)
        {
            char c = telefone[i];
            switch (i)
            {
                case 0: if (c != '(')
                        return "";
                    break;
                case 3: if (c != ')')
                        return "";
                    break;
                case 8: if (c != '-')
                        return "";
                    break;
                case 13: if (c == '_')
                        return telefone.Substring(0, telefone.Length - 1);
                    if (c > '9' || c < '0')
                        return "";
                    break;
                default:
                    if (c > '9' || c < '0')
                        return "";
                    break;
            }
        }


        return telefone;
    }

    public static string converteTelefone(string telefone)
    {
        try
        {
            telefone = telefone.Insert(0, "(");
            telefone = telefone.Insert(3, ")");
            telefone = telefone.Insert(8, "-");

            return telefone;
        }
        catch (Exception ex)
        {
            throw new Exception("Telefone inválido.");
        }
    }

    public static string converteHora(string hora)
    {
        try
        {
            hora = hora.Insert(2, ":");
            return hora;
        }
        catch (Exception ex)
        {
            throw new Exception("Hora inválida.");
        }
    }
    public static string converteData(string data)
    {
        try
        {
            data = data.Insert(2, "/");
            data = data.Insert(5, "/");
            return data;
        }
        catch (Exception ex)
        {
            throw new Exception("Data inválida.");
        }
    }

    public static Boolean validaCEP(String CEP)
    {
        //Formato: 99999-999
        //         012345678

        if (CEP.Length != 9)
            return false;

        for (int i = 0; i < CEP.Length; i++)
        {
            char c = CEP[i];
            switch (i)
            {
                case 5: if (c != '-')
                        return false;
                    break;
                default:
                    if (c > '9' || c < '0')
                        return false;
                    break;
            }
        }

        return true;
    }
    public static Boolean validaCPFCNPJ(String CPFCNPJ)
    {
        //Formato: 999.999.999-99
        //         01234567890123
        CPFCNPJ = CPFCNPJ.Replace("_", "");
        if (CPFCNPJ.Length != 14 && CPFCNPJ.Length != 11)
            return false;

        for (int i = 0; i < CPFCNPJ.Length; i++)
        {
            if (CPFCNPJ[i] > '9' || CPFCNPJ[i] < '0')
                return false;
        }

        return true;
    }

    public static Boolean validaCNPJ(String cnpj)
    {

        if (Regex.IsMatch(cnpj, @"(^(\d{2}.\d{3}.\d{3}/\d{4}-\d{2})|(\d{14})$)"))
        {

            return true;

        }

        else
        {

            return false;

        }

    }

    public static Boolean validaCPF(String cpf)
    {

        if (Regex.IsMatch(cpf, @"(^(\d{3}.\d{3}.\d{3}-\d{2})|(\d{11})$)"))
        {

            return true;

        }

        else
        {

            return false;

        }

    }

    public static Boolean validaNumero(String numero)
    {
        //Formato: 9*
        //         


        if (String.IsNullOrEmpty(numero))
            return false;

        for (int i = 0; i < numero.Length; i++)
        {
            char c = numero[i];
            if (c > '9' || c < '0')
                return false;
        }

        return true;
    }
    public static Boolean validaEmail(String email)
    {
        //Formato: *@*.*
        //         01234567890123
        if (String.IsNullOrEmpty(email))
            return false;

        bool arroba = false;
        bool pontoDepoisDoArroba = false;
        for (int i = 0; i < email.Length; i++)
        {
            char c = email[i];
            if (c == '@')
                arroba = true;
            if (c == '.')
                if (arroba)
                {
                    pontoDepoisDoArroba = true;
                    break;
                }
        }

        if (arroba && pontoDepoisDoArroba)
            return true;
        return false;
    }

    public static Boolean validaLogin(String login)
    {
        if (repUsuario.All().Any(x => x.login == login))
            return false;
        return true;
    }

    //public static Boolean emailDisponivel(String email)
    //{
    //    UsuarioVO usuario = repUsuario.All().Where(x => x.cliente.contato.email1 == email).FirstOrDefault();
    //    if (usuario != null)
    //        if (!usuario.Equals(ControleLogin.usuarioLogin))
    //            return false;
    //    return true;
    //}

    //public static Boolean cpfcnpjDisponivel(String cpfcnpj)
    //{

    //    cpfcnpj = cpfcnpj.Replace("_", "");
    //    ClienteVO cliente = repCliente.All().Where(x => x.CPFCNPJ == cpfcnpj).FirstOrDefault();


    //    if (cliente != null)
    //    {
    //        if (HttpContext.Current.Session["UsuarioLogado"] != null)
    //        {
    //            if (cliente.id != ((UsuarioVO)HttpContext.Current.Session["UsuarioLogado"]).cliente.id)
    //                return false;
    //        }
    //        else return false;
    //    }


    //    return true;

    //}
}