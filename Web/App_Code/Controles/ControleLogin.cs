﻿using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using Modelos;

/// <summary>
/// Summary description for ControleLogin
/// </summary>
public class ControleLogin
{
    public ControleLogin()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    private static Repository<UsuarioVO> repUsuario
    {
        get
        {
            return new Repository<UsuarioVO>(NHibernateHelper.CurrentSession);
        }
    }

    public static UsuarioVO usuarioLoginGerenciador
    {
        get
        {
            statusLoginGerenciador();
            return (UsuarioVO)HttpContext.Current.Session["login"];
        }
    }

    public static bool adminLogado() 
    {
        if (HttpContext.Current.Session["login"] != null)
            return true;
        return false;
    }

    public static UsuarioVO usuarioLogin
    {
        get
        {
            statusLogin();
            UsuarioVO usuario = (UsuarioVO)HttpContext.Current.Session["UsuarioLogado"];
            return usuario;
        }
    }

    //Pagina para onde irá ao se logar e não ter acessado nenhuma outra página que necessite login antes
    private static string PAGINA_DESTINO
    {
        get
        {
            return BaseURL + "/AreaDoUsuario/";
        }
    }
    private static string PAGINA_DESTINO_GERENCIADOR
    {
        get
        {
            return BaseURL + "/Controle/Login.aspx";
        }
    }
    private static string PAGINA_LOGIN
    {
        get
        {
            return BaseURL + "/AreaDoUsuario/Login.aspx";
        }
    }
    private static string PAGINA_LOGIN_GERENCIADOR
    {
        get
        {
            return BaseURL + "/Controle/Default.aspx";
        }
    }

    /// <summary>
    /// Returns the name of the virtual folder where our project lives
    /// </summary>
    /// 
    public static string BaseURL
    {
        get
        {
            try
            {
                string retorno = VirtualPathUtility.ToAbsolute("~/");

                return retorno.Remove(retorno.Length - 1);
            }
            catch
            {
                // This is for design time
                return null;
            }
        }
    }

    /// <summary>
    /// Returns the name of the virtual folder where our project lives
    /// </summary>
    /// 
    private static string VirtualFolder
    {
        get { return HttpContext.Current.Request.ApplicationPath; }
    }

    /// <summary>
    /// Returns the name of the virtual folder where our project lives
    /// </summary>
    /// 
    //public static Boolean recuperaSenhaViaEmail(string email)
    //{

    //    if (HttpContext.Current.Session["TentativasRecuperacao"] != null)
    //    {
    //        int tentativas = (int)HttpContext.Current.Session["TentativasRecuperacao"];
    //        if (tentativas < 5)
    //        {
    //            tentativas++;
    //            HttpContext.Current.Session["TentativasRecuperacao"] = tentativas;
    //        }
    //        else
    //            throw new Exception("Aguarde alguns minutos e tente novamente.");
    //    }
    //    else
    //    {
    //        HttpContext.Current.Session["TentativasRecuperacao"] = 0;
    //    }

    //    UsuarioVO retorno = repUsuario.All().Fetch(x=>x.cliente).Where(x=>x.cliente.contato.email1 == email).FirstOrDefault();

    //    if (retorno == null)
    //    {
    //        throw new Exception("E-mail não encontrado. Confira suas informações e tente novamente.");
    //    }
    //    else
    //    {

    //        DadoVO dado = MetodosFE.getTela("Configurações de SMTP");

    //        EnvioEmailsVO envio = new EnvioEmailsVO();
    //        ClienteVO cliente = retorno.cliente;
    //        envio.nomeRemetente = Configuracoes.getSetting("NomeSite");
    //        envio.emailRemetente = dado.referencia;
    //        envio.emailDestinatario = cliente.contato.email1;
    //        envio.assuntoMensagem = "Recuperação de senha";

    //        string senhaNova = retorno.senha.Substring(0, 8);

    //        retorno.senha = GetSHA1Hash(senhaNova);
    //        repUsuario.Update(retorno);


    //        //Atribui ao método Body a texto da mensagem
    //        string v_recebe = "";

    //        DadoVO recuperacao = MetodosFE.getTela("Geral-E-mail - Recuperacao Senha");


    //        //v_recebe += "Conforme foi solicitado no site " + Configuracoes.getSetting("NomeSite") + ", estamos enviando seu login e uma nova senha, para ser posteriormente alterada, que se encontram abaixo: <br/><br/>";

    //        string mensagem = "";

    //        mensagem = "Login: " + retorno.login;
    //        mensagem += "<br/>Senha: " + senhaNova;

    //        v_recebe = recuperacao.descricao.Replace("[Mensagem]", mensagem);

    //        envio.conteudoMensagem = v_recebe;


    //        bool recebeu = EnvioEmails.envioemails(envio);

    //        if (recebeu)
    //        {
    //            return true;
    //        }
    //        else
    //        {
    //            throw new Exception("Ocorreram problemas no envio do e-mail. Tente mais tarde.");
    //        }
    //    }
    //    return false;
    //}

    public static void statusLogin()
    {
        try
        {
            if (HttpContext.Current.Session["UsuarioLogado"] == null)
            {
                gravarPagina();
                //MetodosFE.mostraMensagem("É preciso se logar para acessar esta página.");
                HttpContext.Current.Response.Redirect(PAGINA_LOGIN);
            }
        }
        catch (System.Threading.ThreadAbortException ex)
        {
            // Do nothing. ASP.NET is redirecting.
            // Always comment this so other developers know why the exception 
            // is being swallowed.
        }
    }
    public static void statusLoginGerenciador()
    {
        try
        {
            if (HttpContext.Current.Session["login"] == null)
            {
                gravarPaginaGerenciador();
                HttpContext.Current.Response.Redirect(PAGINA_LOGIN_GERENCIADOR);
            }
        }
        catch (System.Threading.ThreadAbortException ex)
        {
            // Do nothing. ASP.NET is redirecting.
            // Always comment this so other developers know why the exception 
            // is being swallowed.
        }
    }

    public static string GetSHA1Hash(string input)
    {
        //SHA1 hash
        SHA1 SHA1Hash = SHA1.Create();

        // Convert the input string to a byte array and compute the hash.
        byte[] data = SHA1Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

        // Create a new Stringbuilder to collect the bytes
        // and create a string.
        StringBuilder sBuilder = new StringBuilder();

        // Loop through each byte of the hashed data 
        // and format each one as a hexadecimal string.
        for (int i = 0; i < data.Length; i++)
        {
            sBuilder.Append(data[i].ToString("x2"));
        }

        // Return the hexadecimal string.
        return sBuilder.ToString();
    }

    public static void alterarSenha(string senhaAtual, string senhaNova)
    {
        try
        {
            if (HttpContext.Current.Session["UsuarioLogado"] == null)
                HttpContext.Current.Response.Redirect(PAGINA_LOGIN);

            UsuarioVO usuario = (UsuarioVO)HttpContext.Current.Session["UsuarioLogado"];

            if (usuario.senha == GetSHA1Hash(senhaAtual))
            {

                usuario.senha = GetSHA1Hash(senhaNova);
                repUsuario.Update(usuario);
            }
            else throw new Exception("Senha atual errada.");
        }
        catch (System.Threading.ThreadAbortException ex)
        {
            // Do nothing. ASP.NET is redirecting.
            // Always comment this so other developers know why the exception 
            // is being swallowed.
        }
    }

    public static void login(string login, string senha)
    {
        if (HttpContext.Current.Session["TentativasLogin"] != null)
        {
            int tentativas = (int)HttpContext.Current.Session["TentativasLogin"];
            if (tentativas < 5)
            {
                tentativas++;
                HttpContext.Current.Session["TentativasLogin"] = tentativas;
            }
            else
                throw new Exception("Ocorreram muitas tentativas de efetuar login. Aguarde alguns minutos e tente novamente.");
        }
        else
        {
            HttpContext.Current.Session["TentativasLogin"] = 0;
        }

        

        if (!String.IsNullOrEmpty(login) || !String.IsNullOrEmpty(senha))
        {
            if (senha.Contains("<") || senha.Contains(">") || senha.Contains("/") || senha.Contains("*") || senha.Contains("--") || senha.Contains("{") || senha.Contains("}") || senha.Contains("\\") || senha.Contains("%") ||
    login.Contains("<") || login.Contains(">") || login.Contains("/") || login.Contains("*") || login.Contains("--") || login.Contains("{") || login.Contains("}") || login.Contains("\\") || login.Contains("%"))
            {
                throw new Exception("Login ou Senha Incorretos");
            }
            else
            {
                string senhaHash = GetSHA1Hash(senha);

                HttpContext.Current.Session["UsuarioLogado"] = repUsuario.All().Where(x => x.login == login && x.senha == senhaHash && x.status == "AT").FirstOrDefault();
                if (HttpContext.Current.Session["UsuarioLogado"] != null)
                {
                    string paginaDestino = getPaginaLogin();
                    if (!String.IsNullOrEmpty(paginaDestino))
                    {

                            HttpContext.Current.Response.Redirect(paginaDestino, false);
                    }
                    else
                    {
                            HttpContext.Current.Response.Redirect(PAGINA_DESTINO, false);
                    }
                }
                else
                {
                    throw new Exception("Login ou Senha Incorretos");
                }
            }
        }
    }

    public static void loginGerenciador(string login, string senha)
    {
        if (!String.IsNullOrEmpty(login) || !String.IsNullOrEmpty(senha))
        {
            if (senha.Contains("<") || senha.Contains(">") || senha.Contains("/") || senha.Contains("*") || senha.Contains("--") || senha.Contains("{") || senha.Contains("}") || senha.Contains("\\") || senha.Contains("%") ||
    login.Contains("<") || login.Contains(">") || login.Contains("/") || login.Contains("*") || login.Contains("--") || login.Contains("{") || login.Contains("}") || login.Contains("\\") || login.Contains("%"))
            {
                throw new Exception("Login ou Senha Incorretos");
            }
            else
            {
                List<String> tiposAdm = new List<string>();
                tiposAdm.Add("AA");
                tiposAdm.Add("AD");

                string senhaHash = GetSHA1Hash(senha);
                HttpContext.Current.Session["login"] = repUsuario.All().Where(x => x.login == login && senhaHash == x.senha && x.status == "AT" && tiposAdm.Contains(x.tipo)).FirstOrDefault();

                if (HttpContext.Current.Session["login"] != null)
                {
                    HttpContext.Current.Session["MensagemString"] = null;
                    string paginaDestino = getPaginaLoginGerenciador();
                    if (!String.IsNullOrEmpty(paginaDestino))
                    {
                        HttpContext.Current.Response.Redirect(paginaDestino, false);
                    }
                    else HttpContext.Current.Response.Redirect(PAGINA_DESTINO_GERENCIADOR, false);
                }
                else
                {
                    throw new Exception("Login ou Senha Incorretos");
                }
            }
        }
    }


    protected static string getPaginaLogin()
    {

        return (String)HttpContext.Current.Session["PaginaLogin"];

        if (HttpContext.Current.Session["PaginaLogin"] != null)
            return ((HttpContext.Current.Session["PaginaLogin"].ToString().ToUpper().Contains("ASMX")) ? null : HttpContext.Current.Session["PaginaLogin"].ToString());

        return null;

    }
    protected static string getPaginaLoginGerenciador()
    {
        if (HttpContext.Current.Session["PaginaLoginGerenciador"] != null)
            return ((HttpContext.Current.Session["PaginaLoginGerenciador"].ToString().ToUpper().Contains("ASMX")) ? null : HttpContext.Current.Session["PaginaLoginGerenciador"].ToString());

        return null;

    }
    public static void gravarPagina()
    {
        HttpContext.Current.Session["PaginaLogin"] = !HttpContext.Current.Request.AppRelativeCurrentExecutionFilePath.ToUpper().Contains("ASPX") ? null : HttpContext.Current.Request.Url.OriginalString;
    }



    protected static void gravarPaginaGerenciador()
    {
        HttpContext.Current.Session["PaginaLoginGerenciador"] = !HttpContext.Current.Request.AppRelativeCurrentExecutionFilePath.ToUpper().Contains("ASPX") ? null : HttpContext.Current.Request.Url.OriginalString;
    }



}