﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using Modelos;
using NHibernate;
using NHibernate.Cache;
using NHibernate.Tool.hbm2ddl;
using Repository;
using Repository.Conventions;
using System;
using System.Diagnostics;
using System.Web;
public class NHibernateHelper
{
    public static ISessionFactory SessionFactory
    {
        set
        {

            HttpContext.Current.Application["SessionFactory"] = value;

        }
        get
        {

            if (HttpContext.Current.Application["SessionFactory"] == null)
            {
                HttpContext.Current.Application.Lock();
                HttpContext.Current.Application["SessionFactory"] = CreateSessionFactory();
                HttpContext.Current.Application.UnLock();
            }
            return (ISessionFactory)HttpContext.Current.Application["SessionFactory"];
        }
    }

    public static ISession OpenSession()
    {
        Debug.WriteLine("Session Created");
        return NHibernateHelper.SessionFactory.OpenSession();

    }

    public static ISession CurrentSession
    {
        get { return (ISession)HttpContext.Current.Items["current.session"]; }
        set { HttpContext.Current.Items["current.session"] = value; }
    }

    public static ISessionFactory CreateSessionFactory()
    {
        try
        {
            string connectionString = @"Database=modelo3auto;Data Source=localhost;User Id=root;Password=root;charset=utf8;";

            FluentNHibernate.Cfg.FluentConfiguration configuracaoFNH = null;
            if (System.Diagnostics.Debugger.IsAttached)
            {
                string complemento = "1";
                connectionString = @"Database=" + Configuracoes.getSetting("BDdatabase" + complemento) + ";Data Source=" + Configuracoes.getSetting("BDdatasource" + complemento) + ";User Id=" + Configuracoes.getSetting("BDuser" + complemento) + ";Password=" + Configuracoes.getSetting("BDpassword" + complemento) + ";charset=utf8;";
                configuracaoFNH = Fluently.Configure().ExposeConfiguration(cfg => new SchemaUpdate(cfg).Execute(false, true)).Database(
            MySQLConfiguration.Standard.ConnectionString(connectionString).ShowSql().FormatSql()).ExposeConfiguration(x =>
            {
                x.SetInterceptor(new SqlStatementInterceptor());
            });
            }
            else
            {
                string complemento = "";
                connectionString = @"Database=" + Configuracoes.getSetting("BDdatabase" + complemento) + ";Data Source=" + Configuracoes.getSetting("BDdatasource" + complemento) + ";User Id=" + Configuracoes.getSetting("BDuser" + complemento) + ";Password=" + Configuracoes.getSetting("BDpassword" + complemento) + ";charset=utf8;";
                configuracaoFNH = Fluently.Configure().Database(
            MySQLConfiguration.Standard.ConnectionString(connectionString).ShowSql().FormatSql()).ExposeConfiguration(x =>
            {
                x.SetInterceptor(new SqlStatementInterceptor());
            });
            }


            //FluentNHibernate.Cfg.FluentConfiguration configuracaoFNH = Fluently.Configure().ExposeConfiguration(cfg => new SchemaExport(cfg).Execute(false, true,false)).Database(
            //MySQLConfiguration.Standard.ConnectionString(connectionString).ShowSql().FormatSql()).ExposeConfiguration(x =>
            //{
            //    x.SetInterceptor(new SqlStatementInterceptor());
            //});



            //  configuracaoFNH.Cache(c => c.ProviderClass(typeof(NHibernate.Cache.HashtableCacheProvider).AssemblyQualifiedName)
            //.UseQueryCache());





            configuracaoFNH.Cache(c => c
    .UseQueryCache()
    .ProviderClass<HashtableCacheProvider>());
            configuracaoFNH.Mappings(val => val.AutoMappings.Add(AutoMap.AssemblyOf<DadoVO>(new AutomappingConfiguration()).Conventions.Setup(con =>
            {
                con.Add<DefaultTableNameConvention>();
                con.Add<DefaultPrimaryKeyConvention>();
                con.Add<DefaultStringLengthConvention>();
                con.Add<DefaultReferenceConvention>();
                con.Add<DefaultHasManyConvention>();
            })//.UseOverridesFromAssemblyOf<ProdutoOverride>()
            )

                    );

            return configuracaoFNH.BuildSessionFactory();
        }
        catch (Exception ex)
        {
            throw;
        }
    }
}