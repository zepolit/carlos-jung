﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;


    public class EnvioEmailsVO
    {
        public string nomeRemetente { get; set; }
        public string emailRemetente { get; set; }
        public string emailResposta { get; set; }
        public string emailDestinatario { get; set; }
        public string emailComCopia { get; set; }
        public string emailComCopiaOculta { get; set; }
        public string assuntoMensagem { get; set; }
        public string conteudoMensagem { get; set; }


        
    }


