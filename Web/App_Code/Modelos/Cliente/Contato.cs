﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using Modelos;

namespace Modelos
{

//    create table Categorias
//(
//id_categoria int,
//ds_categoria varchar(100),
//idSegmentoFilho int
//);



    public class ContatoVO: ModeloBase
    {
        // Atributos
        public virtual string email1 { get; set; }
        public virtual string email2 { get; set; }
        public virtual string responsavelEmail1 { get; set; }
        public virtual string responsavelEmail2 { get; set; }
        public virtual string fone1 { get; set; }
        public virtual string fone2 { get; set; }
        public virtual string fone3 { get; set; }
        public virtual string responsavelTelefones { get; set; }

        public ContatoVO() { }

    }
}


