﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using Modelos;

namespace Modelos
{


    public class ClienteVO: ModeloBase, IEquatable<ClienteVO>
    {
        public virtual string nome { get; set; }
        public virtual string razaoSocial { get; set; }
        public virtual string inscricaoEstadual { get; set; }
        public virtual string CPFCNPJ { get; set; }
        public virtual string status { get; set; }
        public virtual DateTime dataInicio { get; set; }
        public virtual DateTime? dataFim { get; set; }
        public virtual string observacao { get; set; }
        public virtual int idLoja { get; set; }

        public virtual ContatoVO contato { get; set; }
        public virtual EnderecoVO endereco { get; set; }


        public ClienteVO() { }





        public virtual bool Equals(ClienteVO other)
        {
            if (other.id == id && id > 0)
                return true;
            if (other.nome.Equals(nome))
                return true;

            return false;
        }
    }
}


