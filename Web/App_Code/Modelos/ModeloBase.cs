﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace Modelos
{
    [Serializable]
    public abstract class ModeloBase: IEquatable<ModeloBase>
    {
        public virtual int id { get; set; }

        public virtual bool Equals(ModeloBase other)
        {
            if (other.id == id && id > 0)
                return true;
            return false;
        }
    }
}