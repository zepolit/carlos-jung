﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
namespace Modelos
{

//    create table Categorias
//(
//id_categoria int,
//ds_categoria varchar(100),
//idSegmentoFilho int
//);



    public class EnderecoVO: ModeloBase
    {
        // Atributos
        public virtual int numero { get; set; }
        public virtual string bairro { get; set; }
        public virtual string endereco { get; set; }
        public virtual string complemento { get; set; }
        public virtual string CEP { get; set; }
        public virtual CidadeVO cidade { get; set; }
        public virtual EstadoVO estado { get; set; }


        //public virtual int idEstado { get {
        //    if (estado != null)
        //        return estado.id;
        //    return 0;
        //}
        //    set
        //    {
        //        estado = new EstadoVO();
        //        estado.id = value;
        //    }
        //}
        //public virtual int idCidade
        //{
        //    get
        //    {
        //        if (cidade != null)
        //            return cidade.id;
        //        return 0;
        //    }
        //    set
        //    {
        //        cidade = new CidadeVO();
        //        cidade.id = value;
        //    }
        //}
        public EnderecoVO()
        { }



    }
}


