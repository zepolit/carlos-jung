﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;

namespace Modelos
{
    public class EstadoVO: ModeloBase, IEquatable<EstadoVO>
    {
        public virtual string nome { get; set; }
        public virtual string sigla { get; set; }

        public EstadoVO()
        { }


        public virtual bool Equals(EstadoVO other)
        {
            if (id == other.id&&id>0)
                return true;
            if (nome.Equals(other.nome))
                return true;
            return false;
        }
    }
}


