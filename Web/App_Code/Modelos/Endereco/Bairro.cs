﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;

namespace Modelos
{

//    create table Categorias
//(
//id_categoria int,
//ds_categoria varchar(100),
//idSegmentoFilho int
//);



    public class BairroVO : ModeloBase, IEquatable<BairroVO>
    {
        // Atributos
        public virtual string nome { get; set; }
        public virtual CidadeVO cidade { get; set; }
        //public virtual int idRegiao { get; set; }

        // Propriedades
        public BairroVO()
        { }



        public virtual bool Equals(BairroVO other)
        {
            if (other.id == id && id > 0)
                return true;
            return false;
        }
    }
}


