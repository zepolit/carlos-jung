﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using Modelos;

namespace Modelos
{
    public class CidadeVO : ModeloBase, IEquatable<CidadeVO>
    {
        public virtual int id { get; set; }
        public virtual string nome { get; set; }
        public virtual EstadoVO estado { get; set; }
        public virtual string status { get; set; }

        //public virtual int idEstado 
        //{
        //    set {
        //        estado = new EstadoVO();
        //        estado.id = value;
        //    }
        //    get
        //    {

        //        if (estado == null)
        //            return 0;
        //        return estado.id;
        //    }
        //}

            public CidadeVO()
            {
            }


            public virtual bool Equals(CidadeVO other)
            {
                if (id == other.id && id>0)
                    return true;
                if (nome.Equals(other.nome) && other.estado.Equals(estado))
                    return true;
                return false;
            }
    }
}


