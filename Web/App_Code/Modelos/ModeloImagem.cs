﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ModeloImagem
/// </summary>
///      
namespace Modelos
{
    public abstract class ModeloImagem 
    {
        public virtual int id { get; set; }
        public virtual string nome { get; set; }
        public virtual int ordem { get; set; }

        // Propriedades

        public virtual string getEnderecoImagemHQ()
        {
            return MetodosFE.BaseURL + "/ImagensHQ/" + nome;
        }
        public virtual string getEnderecoImagemLQ()
        {
            return MetodosFE.BaseURL + "/ImagensLQ/" + nome;
        }

        public virtual void excluirArquivos()
        {
            if (File.Exists(uplImage.diretorioHQ + "\\" + nome))
                File.Delete(uplImage.diretorioHQ + "\\" + nome);

            if (File.Exists(uplImage.diretorioLQ + "\\" + nome))
                File.Delete(uplImage.diretorioLQ + "\\" + nome);
        }
    }
}