﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using FluentNHibernate.Mapping;

namespace Modelos
{
    public class GrupoDePaginasVO: ModeloBase
    {
        public virtual string nome { get; set; }
        public virtual int ordem { get; set; }
        public virtual IList<PaginaDeControleVO> paginas { get; set; }
        public virtual IList<PermissaoGrupoDePaginasVO> permissoes { get; set; }

        public GrupoDePaginasVO() { }



    }

}



