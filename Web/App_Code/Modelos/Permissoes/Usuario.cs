﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using System.Runtime.Serialization;

namespace Modelos
{
    [Serializable]
    public class UsuarioVO: ModeloBase,IEquatable<UsuarioVO>
    {
        public virtual string nome { get; set; }
        public virtual string login { get; set; }
        public virtual string senha { get; set; }
        public virtual string tipo { get; set; }
        public virtual string status { get; set; }
        public virtual ISet<PermissaoGrupoDePaginasVO> permissoesGrupos { get; set; }
        public virtual ISet<PermissaoVO> permissoesPaginas { get; set; }


        public UsuarioVO() { }



        public virtual bool Equals(UsuarioVO other)
        {
            if (id == other.id && id > 0)
                return true;
            if (login.Equals(other.login))
                return true;
            return false;
        }
    }
}


