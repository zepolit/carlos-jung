﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using Modelos;

namespace Modelos
{

    public class SegmentoPaiVO: ModeloBase
    {
        // Atributos
        public virtual string nome { get; set; }
        public virtual string chave { get; set; }
        public virtual Boolean visivel { get; set; }
        public virtual string descricao { get; set; }
        public virtual string ordem { get; set; }
        public virtual Tela tela { get; set; }
        public virtual IList<ImagemSegPaiVO> imagens { get; set; }

        public SegmentoPaiVO()
        { }

    }
}


