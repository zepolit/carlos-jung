﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using Modelos;

namespace Modelos
{



    public class CategoriaVO :ModeloBase, IEquatable<CategoriaVO>
    {
        // Atributos
        public virtual string nome { get; set; }
        public virtual string imagem { get; set; }
        public virtual string descricao { get; set; }
        public virtual string ordem { get; set; }
        public virtual string chave { get; set; }
        public virtual Tela tela { get; set; }
        public virtual IList<ImagemCategoriaVO> imagens { get; set; }
        public virtual SegmentoFilhoVO segFilho { get; set; }
        public virtual bool visivel { get; set; }


        public virtual string getPrimeiraImagemHQ()
        {
            if (!String.IsNullOrEmpty(imagem))
                return MetodosFE.BaseURL + "/ImagensHQ/" + imagem;
            else
                return uplImage.imgSemImagem;
        }



        // Propriedades
        public CategoriaVO()
        { }

        public virtual bool Equals(CategoriaVO other)
        {
            if (id == other.id && id >0)
                return true;
            if (nome.Equals(other.nome) && other.segFilho.Equals(segFilho))
                return true;
            return false;
        }
    }
}


