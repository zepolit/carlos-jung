﻿using NHibernate;
using System;
using System.Diagnostics;

public class SqlStatementInterceptor : EmptyInterceptor
{
    public override NHibernate.SqlCommand.SqlString OnPrepareStatement(NHibernate.SqlCommand.SqlString sql)
    {
        Debug.WriteLine(base.OnPrepareStatement(sql));
        Debug.WriteLine("-------------------------------------");
        return sql;
    }
}