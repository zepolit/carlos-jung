﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Linq;
using System.Collections.Generic;
using Modelos;

public partial class _Default : System.Web.UI.Page
{

   protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = "Home - " + Configuracoes.getSetting("NomeSite");

      if (!IsPostBack)
      {
         DadoVO dadoAnimacao = null;
         dadoAnimacao = MetodosFE.documentos.FirstOrDefault(x => x.tela.nome.Equals("Animação Home"));

         if (dadoAnimacao != null)
         {
            repImagens.DataSource = dadoAnimacao.getImagensOrdenadas();
            repImagens.DataBind();
         }


         IList<DadoVO> dadoNoticias = null;
         dadoNoticias = MetodosFE.documentos.Where(x => x.tela.nome.Equals("Notícias") && x.visivel).Take(4).OrderBy(x => x.data).ToList();

         if (dadoNoticias != null && dadoNoticias.Count > 0)
         {
            repNoticias.DataSource = dadoNoticias;
            repNoticias.DataBind();
         }

         IList<DadoVO> menusHome = null;
         menusHome = MetodosFE.documentos.Where(x => x.tela.nome.Equals("Menus Home") && x.visivel).OrderBy(x => x.ordem).Take(3).ToList();

         if (menusHome != null && menusHome.Count > 0)
         {
             repMenusHome.DataSource = menusHome;
             repMenusHome.DataBind();         
         }

         DadoVO dadoChamadaComercial = null;

         dadoChamadaComercial = MetodosFE.getTela("Chamada Comercial Home");

         if (dadoChamadaComercial != null)
         {
             litTituloChamadaComercial.Text = dadoChamadaComercial.nome;
             litTextoChamadaComercial.Text = dadoChamadaComercial.descricao;
         }
      }
   }
}


