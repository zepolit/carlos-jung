﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="noticia.aspx.cs" Inherits="_noticia" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


      

     <!--=== CONTEÚDO ===-->
        <div class="fundo-area-de-atuacao"></div>

        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 area-de-atuacao">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 area-de-atuacao-conteudo">
                        <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 area-de-atuacao-titulo">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 area-de-atuacao-titulo">
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 area-de-atuacao-titulo">
                                    <h1 class="font-robotolight">
                                        <asp:Literal runat="server" ID="litTitulo"/>
                                    </h1>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 area-de-atuacao-titulo">
                                    <a href="javascript:window.history.go(-1)" class=" rounded voltar-detalhe-da-noticia pull-right">
                                        Voltar
                                    </a>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 area-de-atuacao-texto detalhe-texto">
                              <asp:Literal runat="server" ID="litTexto"/>

                            </div>
                        </div>
                        <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 detalhe-img">
                            <img src="#" runat="server" id="imgNoticia" class="img-responsive" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--=== FIM CONTEÚDO ===-->




</asp:Content>
