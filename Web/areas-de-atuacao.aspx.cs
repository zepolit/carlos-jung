﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Linq;
using System.Collections.Generic;
using Modelos;

public partial class _areasDeAtuacao : System.Web.UI.Page
{
   protected void Page_Load(object sender, EventArgs e)
   {
       Page.Title = "Áreas de Atuação - " + Configuracoes.getSetting("NomeSite");

      if (!IsPostBack)
      {
         IList<DadoVO> dadoMenus = null;
         dadoMenus = MetodosFE.documentos.Where(x => x.tela.nome.Equals("Áreas de Atuação") && x.visivel).OrderBy(x => x.nome).ToList();

         if (dadoMenus != null && dadoMenus.Count > 0)
         {
            repMenu.DataSource = dadoMenus;
            repMenu.DataBind();
         }

         if (RouteData.Values["Chave"] != null)
         {
            string chave = RouteData.Values["Chave"].ToString();

            DadoVO dadoAreaAtuacao = null;

            dadoAreaAtuacao = MetodosFE.documentos.FirstOrDefault(x => x.tela.nome.Equals("Áreas de Atuação") && x.chave.Equals(chave));

            if (dadoAreaAtuacao != null)
            {
               litTitulo.Text = dadoAreaAtuacao.nome;
               litTexto.Text = dadoAreaAtuacao.descricao;
            }
         }
         else
         {
            DadoVO dadoDefault = dadoMenus[0];

            if (dadoDefault != null)
            {
               litTitulo.Text = dadoDefault.nome;
               litTexto.Text = dadoDefault.descricao;

            }
         }
      }
   }
}
