﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Linq;
using System.Collections.Generic;
using Modelos;

public partial class _quemSomos : System.Web.UI.Page
{
   public string cont = null;


   protected void Page_Load(object sender, EventArgs e)
   {
       Page.Title = "Quem Somos - " + Configuracoes.getSetting("NomeSite");

      DadoVO dadoNossaHistoria = null;

      dadoNossaHistoria = MetodosFE.getTela("Nossa História");
      if (dadoNossaHistoria != null)
      {
         litTexto.Text = dadoNossaHistoria.descricao;
         repImgs.DataSource = dadoNossaHistoria.getImagensOrdenadas();
         repImgs.DataBind();
      }
      liNossaHistoria.Attributes["Class"] = "active";
      limissao.Attributes["Class"] = "";
      liParceiros.Attributes["Class"] = "";
      liAdvocacia.Attributes["Class"] = "";
   }

   protected void linkNossaHistoria_Click(object sender, EventArgs e)
    {
      litTexto.Text = "";
      DadoVO dadoNossaHistoria = null;

      dadoNossaHistoria = MetodosFE.getTela("Nossa História");
      if (dadoNossaHistoria != null)
      {
         litTexto.Text = dadoNossaHistoria.descricao;
         repImgs.DataSource = dadoNossaHistoria.getImagensOrdenadas();
         repImgs.DataBind();         
      }

      liNossaHistoria.Attributes["Class"] = "active";
      limissao.Attributes["Class"] = "";
      liParceiros.Attributes["Class"] = "";
      liAdvocacia.Attributes["Class"] = "";
   }

   protected void btnMissao_Click(object sender, EventArgs e)
   {
      litTexto.Text = "";
      DadoVO dadoMissao = null;

      dadoMissao = MetodosFE.getTela("Missão e Valores");
      if (dadoMissao != null)
      {
         litTexto.Text = dadoMissao.descricao;
         repImgs.DataSource = dadoMissao.getImagensOrdenadas();
         repImgs.DataBind();
      }

      limissao.Attributes["Class"] = "active";
      liNossaHistoria.Attributes["Class"] = "";
      liParceiros.Attributes["Class"] = "";
      liAdvocacia.Attributes["Class"] = "";
   }

   protected void btnParceiros_Click(object sender, EventArgs e)
   {
      litTexto.Text = "";
      DadoVO dadoParceiros = null;

      dadoParceiros = MetodosFE.getTela("Parceiros");
      if (dadoParceiros != null)
      {
         litTexto.Text = dadoParceiros.descricao;
         repImgs.DataSource = dadoParceiros.getImagensOrdenadas();
         repImgs.DataBind();
      }

      liParceiros.Attributes["Class"] = "active";
      liNossaHistoria.Attributes["Class"] = "";
      limissao.Attributes["Class"] = "";
      liAdvocacia.Attributes["Class"] = "";
   }

   protected void btnAdvocacia_Click(object sender, EventArgs e)
   {
      litTexto.Text = "";
      DadoVO dadoAdvocacia = null;

      dadoAdvocacia = MetodosFE.getTela("Advocacia");
      if (dadoAdvocacia != null)
      {
         litTexto.Text = dadoAdvocacia.descricao;
         repImgs.DataSource = dadoAdvocacia.getImagensOrdenadas();
         repImgs.DataBind();
      }

      liAdvocacia.Attributes["Class"] = "active";
      liNossaHistoria.Attributes["Class"] = "";
      liParceiros.Attributes["Class"] = "";
      limissao.Attributes["Class"] = "";
   }
}
