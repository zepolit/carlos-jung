﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Linq;
using System.Collections.Generic;
using Modelos;

public partial class _equipe : System.Web.UI.Page
{
   protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = "A Equipe - " + Configuracoes.getSetting("NomeSite");

      if (!IsPostBack)
      {
          DadoVO dadoTextoEquipe = null;
          dadoTextoEquipe = MetodosFE.getTela("Texto");
          
          if (dadoTextoEquipe != null)
          {
              litTexto.Text = dadoTextoEquipe.descricao;            
          }


         IList<DadoVO> dadoEquipe = null;

         dadoEquipe = MetodosFE.documentos.Where(x => x.tela.nome.Equals("Membros") && x.visivel).OrderBy(x => x.ordem).ToList();

         if (dadoEquipe != null && dadoEquipe.Count > 0)
         {
            repEquipe.DataSource = dadoEquipe;
            repEquipe.DataBind();
         }
      }
   }
}
