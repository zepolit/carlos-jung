﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Globalization;
using System.Linq;
using NHibernate.Linq;
using Modelos;
using System.Windows.Forms;
using System.Drawing;
using System.Collections.Generic;

public partial class MasterPage : System.Web.UI.MasterPage
{
    protected void Page_Init(object sender, EventArgs e)
    {
    }

    public string MetaTagsSociais { get; set; }
    protected void Page_Load(object sender, EventArgs e)
    {
        form1.Action = Request.RawUrl;
        string mensagem = MetodosFE.confereMensagem();
        litErro.Text = mensagem != null ? mensagem : "";

        if (!IsPostBack)
        {
            string url = Request.Url.GetLeftPart(UriPartial.Path);

            DadoVO dados = MetodosFE.documentos.FirstOrDefault(x => x.tela.nome == "MetaTags por Página" && !String.IsNullOrEmpty(x.nome) && x.nome.Equals(url));
            if (dados != null)
            {
                litMetaTags.Text = dados.descricao;
            }
            else
            {
                dados = MetodosFE.documentos.FirstOrDefault(x => x.tela.nome == "MetaTags Principal");
                if (dados != null)
                {
                    litMetaTags.Text = dados.descricao;
                    litGetSocial.Text = dados.meta;
                }
            }

            dados = MetodosFE.getTela("Analytics");
            if (dados != null)
                litAnalytics.Text = dados.descricao;

            dados = MetodosFE.getTela("Barra GetSocial");
            if (dados != null)
                litGetSocial.Text = dados.descricao;

            //if (ViewState["Metas"] != null)
            //    litSocialTags.Text = ViewState["Metas"].ToString();


        }
        if (String.IsNullOrEmpty(Page.Title))
        {
            Page.Title = ".: " + Configuracoes.getSetting("NomeSite") + " :.";
        }
    }


    public void configuraTags(string tags)
    {
        MetaTagsSociais = tags;
    }
    protected virtual void Page_LoadComplete(object sender, EventArgs e)
    {
        string mensagem = MetodosFE.confereMensagem();
        litErro.Text = mensagem != null ? mensagem : "";
    }


}
