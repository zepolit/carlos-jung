﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Linq;
using System.Collections.Generic;
using Modelos;

public partial class _noticia : System.Web.UI.Page
{
   protected void Page_Load(object sender, EventArgs e)
   {
      if (!IsPostBack)
      {

         if (RouteData.Values["Chave"] != null)
         {
            string chave = RouteData.Values["Chave"].ToString();

            DadoVO dadoNoticia = null;
            dadoNoticia = MetodosFE.documentos.FirstOrDefault(x => x.tela.nome.Equals("Notícias") && x.chave.Equals(chave));

            if (dadoNoticia != null)
            {
               litTitulo.Text = dadoNoticia.nome;
               litTexto.Text = dadoNoticia.descricao;
               imgNoticia.Src = dadoNoticia.getPrimeiraImagemHQ();
               imgNoticia.Alt = dadoNoticia.nome;
            }
         }
      }
   }
}
